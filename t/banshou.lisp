(in-package :cl-user)
(defpackage shinrabanshou-test.banshou
  (:use #:cl
        #:upanishad
        #:shinrabanshou
        #:prove
        #:shinrabanshou-test.utility))
(in-package :shinrabanshou-test.banshou)

(plan nil)

(subtest "make-banshou"
  (let* ((store-dir (make-store-dir "banshou"))
         (banshou (make-banshou 'banshou store-dir)))
    (ok banshou)))

(subtest "statistics-vertexes")

(subtest "statistics-edges")

(subtest "statistics-indexs"
  (let* ((store-dir (make-store-dir "banshou"))
         (banshou (make-banshou 'banshou store-dir)))
    (print (shinrabanshou::statistics-indexs banshou))))

(finalize)
