(in-package :cl-user)
(defpackage shinrabanshou-test.utility
  (:use #:cl
        #:upanishad
        #:shinrabanshou
        #:prove)
  (:export #:vertex
           #:name
           #:edge)
  (:export #:make-store-dir
           #:with-banshou)
  (:export #:is-class
           #:is-%id
           #:is-%ids))
(in-package :shinrabanshou-test.utility)

;;;;;
;;;;; classes
;;;;;
(defclass vertex (shin)
  ((name :documentation ""
         :accessor name
         :initarg :name
         :initform nil)))

(defclass edge (ra) ())


;;;;;
;;;;; with *
;;;;;
(defun make-store-dir (name)
    (merge-pathnames (format nil "t/data/~a/" name)
                     (asdf:system-source-directory :shinrabanshou-test)))

(defmacro with-banshou ((graph name) &body body)
  `(let ((,graph nil))
     (unwind-protect
          (progn
            (setf ,graph (make-banshou 'banshou (make-store-dir ,name)))
            ,@body))))

;;;;;
;;;;; matcher
;;;;;
(defun is-class (got expect)
  (is (class-name (class-of got))
      expect))

(defun is-%id (got expect)
  (is (%id got)
      (%id expect)))

(defun is-%ids (gots expects)
  (labels ((%ids (memes)
             (sort (mapcar #'%id memes) #'<)))
    (is (%ids gots)
        (%ids expects))))
