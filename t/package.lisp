(in-package :cl-user)
(defpackage shinrabanshou-test
  (:use #:cl
        #:upanishad
        #:shinrabanshou
        #:prove
        #:shinrabanshou-test.utility)
  (:nicknames :shinra-test))
(in-package :shinrabanshou-test)

;; (setf prove:*default-reporter* :dot)
(setf prove:*default-reporter* :list)
