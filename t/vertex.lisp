(in-package :cl-user)
(defpackage shinrabanshou-test.vertex
  (:use #:cl
        #:upanishad
        #:shinrabanshou
        #:prove
        #:shinrabanshou-test.utility))
(in-package :shinrabanshou-test.vertex)

(plan 5)

(subtest "TX-CREATE-VERTEX"
  (with-banshou (graph "banshou")
    (let ((subject (tx-create-vertex graph 'vertex)))
      (ok subject)
      (is-class subject 'vertex))))

(subtest "GET-VERTEX"
  (with-banshou (graph "banshou")
    (let ((vertex (tx-create-vertex graph 'vertex)))
      (let ((subject (get-vertex graph 'vertex :%id (up:%id vertex))))
        (ok subject)
        (is-%id subject vertex)))))

(subtest "FIND-VERTEX"
  (with-banshou (graph "banshou")
    (let ((vertex1 (tx-create-vertex graph 'vertex '((name . "name-1"))))
          (vertex2 (tx-create-vertex graph 'vertex '((name . nil))))
          (vertex3 (tx-create-vertex graph 'vertex '((name . "name-3")))))

      (subtest "all"
        (let ((subjects (find-vertex graph 'vertex)))
          (is-%ids subjects (list vertex1 vertex2 vertex3))))

      (subtest "with slot"
        (let ((subjects (find-vertex graph 'vertex :slot 'name)))
          (is-%ids subjects (list vertex2))))

      (subtest "with slot and value"
        (subtest "value is nil"
          (let ((subjects (find-vertex graph 'vertex :slot 'name :value nil)))
            (is-%ids subjects (list vertex2))))
        (subtest "value is value"
          (let ((subjects (find-vertex graph 'vertex :slot 'name :value "name-3")))
            (is-%ids subjects (list vertex3))))))))

(subtest "TX-DELETE-VERTEX"
  (with-banshou (graph "banshou")
    (let ((vertex (tx-create-vertex graph 'vertex)))
      (ok (get-vertex graph 'vertex :%id (up:%id vertex)))
      (let ((subject (tx-delete-vertex graph vertex)))
        (is-class subject 'banshou)
        (is (get-vertex graph 'vertex :%id (up:%id vertex))
            nil)))))

(subtest "vertexp")

(finalize)
