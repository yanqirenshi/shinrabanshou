(in-package :cl-user)
(defpackage shinrabanshou-test.persistence
  (:use #:cl
        #:upanishad
        #:shinrabanshou
        #:shinrabanshou.persistence
        #:prove
        #:shinrabanshou-test.utility))
(in-package :shinrabanshou-test.persistence)

(plan nil)

(subtest "SHINRABANSHOU-TEST.PERSISTENCE")

(subtest "SNAPSHOT"
  (with-banshou (graph "persistence")
    (let* ((v1 (tx-create-vertex graph 'vertex))
           (v2 (tx-create-vertex graph 'vertex))
           (edge (tx-create-edge graph 'edge v1 v2 :have-to)))
      (declare (ignore edge))
      (is (snapshot graph) graph))))

(finalize)
