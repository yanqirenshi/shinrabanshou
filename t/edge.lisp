(in-package :cl-user)
(defpackage shinrabanshou-test.edge
  (:use #:cl
        #:upanishad
        #:shinrabanshou
        #:prove
        #:shinrabanshou-test.utility))
(in-package :shinrabanshou-test.edge)

(plan nil)

(subtest "edgep")

(subtest "tx-create-edge"
  (with-banshou (graph "banshou")
    (let ((v1 (tx-create-vertex graph 'vertex))
          (v2 (tx-create-vertex graph 'vertex)))
    (is-class (tx-create-edge graph 'edge v1 v2 :have-to)
              'edge))))

(subtest "get-from-vertex"
  (with-banshou (graph "banshou")
    (let ((v1 (tx-create-vertex graph 'vertex))
          (v2 (tx-create-vertex graph 'vertex)))
      (let ((subject (tx-create-edge graph 'edge v1 v2 :have-to)))
        (is (get-from-vertex graph subject)
            v1)))))

(subtest "get-to-vertex"
  (with-banshou (graph "banshou")
    (let ((v1 (tx-create-vertex graph 'vertex))
          (v2 (tx-create-vertex graph 'vertex)))
      (let ((subject (tx-create-edge graph 'edge v1 v2 :have-to)))
        (is (get-to-vertex graph subject)
            v2)))))

(subtest "tx-change-vertex"
  (subtest "from"
    (with-banshou (graph "banshou")
      (let ((v1 (tx-create-vertex graph 'vertex))
            (v2 (tx-create-vertex graph 'vertex))
            (v3 (tx-create-vertex graph 'vertex)))
        (let ((edge (tx-create-edge graph 'edge v1 v2 :have-to)))
          (let ((subject (tx-change-vertex graph edge :from v3)))
            (is (get-from-vertex graph subject) v3)
            (is (get-to-vertex graph subject) v2))))))
  (subtest "to"
    (with-banshou (graph "banshou")
      (let ((v1 (tx-create-vertex graph 'vertex))
            (v2 (tx-create-vertex graph 'vertex))
            (v3 (tx-create-vertex graph 'vertex)))
        (let ((edge (tx-create-edge graph 'edge v1 v2 :have-to)))
          (let ((subject (tx-change-vertex graph edge :to v3)))
            (is (get-from-vertex graph subject) v1)
            (is (get-to-vertex graph subject) v3)))))))

(subtest "tx-change-type"
  (with-banshou (graph "banshou")
    (let ((v1 (tx-create-vertex graph 'vertex))
          (v2 (tx-create-vertex graph 'vertex)))
      (let ((subject (tx-create-edge graph 'edge v1 v2 :have-to)))
        (is (shinra::edge-type (tx-change-type graph subject :have-to-change))
            :have-to-change)))))

(subtest "tx-delete-edge")

(finalize)
