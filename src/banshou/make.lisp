(in-package :shinrabanshou)

(defgeneric make-banshou (class-symbol data-stor)
  (:documentation "")
  (:method ((class-symbol symbol) data-stor)
    (make-pool data-stor
               :pool-class class-symbol
               :snapshot-types '((:vertexes . vertexes)
                                 (:edges    . edges)))))
