(in-package :shinrabanshou)

(defgeneric get-vertexes (graph &key class)
  (:documentation "プールの memes スロットから memes クラスのインスタンスを取得する。")
  (:method ((graph banshou) &key class)
    ;; TODO: upanishad.pool::get-meme に移譲したら？
    (let ((ht (vertexes graph))
          (key class))
      (when ht
        (gethash key ht)))))

(defgeneric tx-add-vertexes (graph memes)
  (:documentation "")
  (:method ((graph banshou) (memes up.memes:memes))
    (upanishad.pool.core::tx-add-memes-core graph memes :memes-slot 'vertexes))
  (:method ((graph banshou) (class symbol))
    (let ((memes (up.memes:make-memes class)))
      (tx-add-vertexes graph memes))))

(defmethod ensure-vertexes ((graph banshou) &key class)
  (or (get-vertexes graph :class class)
      (tx-add-vertexes graph class)))
