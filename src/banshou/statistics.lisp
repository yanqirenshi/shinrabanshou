(in-package :shinrabanshou)

(defun %statics (ht func)
  (with-hash-table-iterator (iterator ht)
    (do ((out nil) (last-p nil))
        (last-p out)
      (multiple-value-bind (entry-p class memes)
          (iterator)
        (if (null entry-p)
            (setf last-p t)
            (push (funcall func class memes)
                  out))))))

(defgeneric statistics-vertexes (graph)
  (:documentation "")
  (:method ((graph banshou))
    (%statics (vertexes graph)
              #'(lambda (class memes)
                  (list :class class
                        :memes memes)))))

(defgeneric statistics-edges (graph)
  (:documentation "")
  (:method ((graph banshou))
    (%statics (edges graph)
              #'(lambda (class memes)
                  (list :class class
                        :memes memes)))))

(defgeneric statistics-indexs (graph)
  (:documentation "")
  (:method ((graph banshou))
    (%statics (upanishad.pool.core::indexes graph)
              #'(lambda (class memes)
                  (list :class class
                        :memes memes)))))
