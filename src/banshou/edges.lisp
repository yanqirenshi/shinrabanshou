(in-package :shinrabanshou)

(defgeneric get-edges (graph &key class)
  (:documentation "プールの memes スロットから memes クラスのインスタンスを取得する。")
  (:method ((graph banshou) &key class)
    ;; TODO: upanishad.pool::get-meme に移譲したら？
    (let ((ht (edges graph))
          (key class))
      (when ht
        (gethash key ht)))))

(defun create-edge-index (graph memes)
  (let ((meme-class (up.memes:meme-class memes)))
    (up:tx-add-index graph (list :class meme-class :slot 'from-id :type :multiple))
    (up:tx-add-index graph (list :class meme-class :slot 'to-id   :type :multiple))))

(defgeneric tx-add-edges (graph memes)
  (:documentation "")
  (:method ((graph banshou) (memes up.memes:memes))
    (up::tx-add-memes-core graph memes :memes-slot 'edges)
    (create-edge-index graph memes)
    memes)
  (:method ((graph banshou) (class symbol))
    (let ((memes (up.memes:make-memes class)))
      (tx-add-edges graph memes))))

(defgeneric ensure-edges (graph &key class)
  (:method ((graph banshou) &key class)
    (or (get-edges graph :class class)
        (tx-add-edges graph class))))
