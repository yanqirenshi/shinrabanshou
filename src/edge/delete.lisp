(in-package :shinrabanshou)

(defun remove-edge-on-index (graph edge)
  (up:tx-remove-index graph (up:get-index graph edge 'from-id))
  (up:tx-remove-index graph (up:get-index graph edge 'to-id))
  (up:tx-remove-index graph (up:get-index graph edge 'edge-type)))

(defun remove-edge-on-memes (graph edge)
  (up:tx-delete-meme graph (class-name (class-of edge)) (%id edge)))

(defgeneric tx-delete-edge (graph edge)
  (:documentation "Vertexを削除します。")
  (:method ((graph banshou) (edge ra))
    (assert-graph graph)
    (remove-edge-on-index graph edge)
    (remove-edge-on-memes graph edge)))
