(in-package :shinrabanshou)

;;;;;
;;;;; Update
;;;;;
(defgeneric tx-update-edge (graph edge-or-class &key %id slots-and-values)
  (:documentation "")
  (:method ((graph banshou) (edge shin) &key %id slots-and-values)
    (declare (ignore %id))
    (up::tx-update-meme-core graph
                             (get-class edge)
                             (up:%id edge)
                             slots-and-values
                             :memes-slot 'edges))
  (:method ((graph banshou) (class symbol) &key %id slots-and-values)
    (assert %id)
    (up::tx-update-meme-core graph
                             class
                             %id
                             slots-and-values
                             :memes-slot 'edges)))


;;;;;
;;;;; change
;;;;;

(defun get-edge-vertex-slot (type)
  (cond ((eq :from type) (values 'from 'from-id 'from-class))
        ((eq :to type)   (values 'to   'to-id   'to-class))
        (t (error "~a, ~a" "edge-slot" type))))

;; vertex
(defgeneric tx-change-vertex (graph edge type vertex)
  (:documentation "edge に関連付いているノードを変更します。
type に fromノードか toノードかを指定します。")
  (:method ((graph banshou) (edge ra) type (vertex shin))
    (assert-graph graph)
    (multiple-value-bind (slot cls-id cls-class)
        (get-edge-vertex-slot type)
      (up::tx-update-meme-core graph edge
                               `((,slot      . ,vertex)
                                 (,cls-id    . ,(%id vertex))
                                 (,cls-class . ,(get-class vertex)))))
    (values edge vertex)))

;; type
(defgeneric tx-change-type (graph edge type)
  (:documentation "")
  (:method ((graph banshou) (edge ra) type)
    (assert-graph graph)
    (assert edge)
    (assert type)
    (up::tx-update-meme-core graph edge
                             `((edge-type . ,type)))))
