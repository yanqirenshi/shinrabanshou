(in-package :shinrabanshou)

(defgeneric find-edge (graph class-symbol &key slot value)
  (:documentation "")
  (:method ((graph banshou) (class-symbol symbol) &key slot value)
    (assert-graph graph)
    (up::find-meme-core graph class-symbol
                        :slot slot
                        :value value
                        :memes-slot 'edges)))

(defgeneric get-edge (graph class-symbol &key %id)
  (:documentation "")
  (:method ((graph banshou) (class-symbol symbol) &key %id)
    (assert-graph graph)
    (cond (%id (up::get-meme-core graph
                                  class-symbol
                                  :%id %id
                                  :memes-slot 'edges))
          (t nil))))


(defgeneric get-from-vertex (graph edge)
  (:documentation "edge のfromノードを取得します。")
  (:method ((pool banshou) (edge ra))
    (get-vertex pool :all :%id (from-id edge))))


(defgeneric get-to-vertex (graph edge)
  (:documentation "edge のtoノードを取得します。")
  (:method ((pool banshou) (edge ra))
    (get-vertex pool :all :%id (to-id edge))))
