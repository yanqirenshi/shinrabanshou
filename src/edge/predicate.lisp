(in-package :shinrabanshou)

(defgeneric edgep (obj)
  (:documentation "symbolで指定された class が edge のサブクラスかどうかを返す。")
  (:method (obj) (declare (ignore obj)) nil)
  (:method ((edge ra)) t)
  (:method ((class-symbol symbol))
    (handler-case
        (edgep (make-instance class-symbol))
      (error () nil))))
