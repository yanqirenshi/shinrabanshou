(in-package :shinrabanshou)

(defun create-edge-params (from to type slots-and-values)
  (let ((params `((from       . ,from)
                  (from-id    . ,(%id from))
                  (from-class . ,(class-name (class-of from)))
                  (to         . ,to)
                  (to-id      . ,(%id to))
                  (to-class   . ,(class-name (class-of to)))
                  (edge-type  . ,type))))
    (if (null slots-and-values)
        params
        (append params slots-and-values))))

(defgeneric tx-create-edge (graph class from to type &optional params)
  (:method ((graph banshou) (class symbol) (from shin) (to shin) (type symbol) &optional params)
    (assert-graph graph)
    (up::tx-create-meme-core graph
                             (ensure-edges graph :class class)
                             class
                             :params (create-edge-params from to type params))))
