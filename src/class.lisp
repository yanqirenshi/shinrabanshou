(in-package :shinrabanshou)

(defclass shin (meme)
  ()
  (:documentation ""))


(defclass ra (meme)
  ((edge-type  :accessor edge-type  :initarg :edge-type  :initform nil)
   ;; from
   (from       :accessor from       :initarg :from       :initform nil)
   (from-id    :accessor from-id    :initarg :from-id    :initform nil)
   (from-class :accessor from-class :initarg :from-class :initform nil)
   ;; to
   (to         :accessor to         :initarg :to-id      :initform nil)
   (to-id      :accessor to-id      :initarg :to-id      :initform nil)
   (to-class   :accessor to-class   :initarg :to-class   :initform nil))
  (:documentation ""))


(defclass banshou (pool)
  ((vertexes
    :documentation ":type hash-table"
    :accessor vertexes
    :initform (make-hash-table :test 'eq)
    :type 'hash-table)
   (edges
    :documentation ":type hash-table"
    :accessor edges
    :initform (make-hash-table :test 'eq)
    :type 'hash-table))
  (:documentation ""))
