(in-package :shinrabanshou)

;;;;;
;;;;; find-vertex
;;;;;
(defgeneric find-vertex (graph class-symbol &key slot value)
  (:documentation "")
  (:method ((graph banshou) (class-symbol symbol) &key slot value)
    (assert-graph graph)
    (up::find-meme-core graph class-symbol
                        :slot slot
                        :value value
                        :memes-slot 'vertexes)))

;;;;;
;;;;; get-vertex
;;;;;
(defun get-vertex-at-%id-class (graph class %id)
  (up::get-meme-core graph
                     class
                     :%id %id
                     :memes-slot 'vertexes))

(defun get-vertex-at-%id-all (graph %id)
  (with-hash-table-iterator (iterator (vertexes graph))
    (do ((vertex nil) (last-p nil))
        ((or vertex last-p) vertex)
      (multiple-value-bind (entry-p class memes)
          (iterator)
        (declare (ignore class))
        (if (null entry-p)
            (setf last-p t)
            (setf vertex (up.memes:get-meme memes :%id %id)))))))

(defun get-vertex-at-%id (graph class %id)
  (if (eq :all class)
      (get-vertex-at-%id-all graph %id)
      (get-vertex-at-%id-class graph class %id)))

(defgeneric get-vertex (graph class &key %id)
  (:documentation "")
  (:method ((graph banshou) (class symbol) &key %id)
    (assert-graph graph)
    (cond (%id (get-vertex-at-%id graph class %id))
          (t nil))))
