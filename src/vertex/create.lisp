(in-package :shinrabanshou)

(defgeneric tx-create-vertex (graph class &optional slots-and-values)
  (:method ((graph banshou) (class symbol) &optional slots-and-values)
    (assert-graph graph)
    (assert-vertex class)
    (up::tx-create-meme-core graph
                             (ensure-vertexes graph :class class)
                             class
                             :params slots-and-values)))
