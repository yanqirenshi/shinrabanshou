(in-package :shinrabanshou)

(defgeneric tx-update-vertex (graph vertex-or-class &key %id slots-and-values)
  (:documentation "")
  (:method ((graph banshou) (vertex shin) &key %id slots-and-values)
    (declare (ignore %id))
    (up::tx-update-meme-core graph
                             (get-class vertex)
                             (up:%id vertex)
                             slots-and-values
                             :memes-slot 'vertexes))
  (:method ((graph banshou) (class symbol) &key %id slots-and-values)
    (assert %id)
    (up::tx-update-meme-core graph
                             class
                             %id
                             slots-and-values
                             :memes-slot 'vertexes)))
