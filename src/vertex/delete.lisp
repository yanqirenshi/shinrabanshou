(in-package :shinrabanshou)

(defgeneric tx-delete-vertex (graph vertex)
  (:documentation "Vertexを削除します。関係を持っている Vertex は削除できません。")
  (:method ((graph banshou) (vertex shin))
    (assert-graph graph)
    (assert-not-exist-edge graph vertex)
    (let ((vertex-class (class-name (class-of vertex))))
      (up::tx-delete-meme-core graph
                               vertex-class (%id vertex)
                               :memes-slot 'vertexes))))
