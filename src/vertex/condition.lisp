(in-package :shinrabanshou)

;;;;;
;;;;; assert-vertex
;;;;;
(defun assert-vertex (class-symbol)
  (assert (vertexp class-symbol)))


;;;;;
;;;;; assert-not-exist-edge
;;;;;
(defun existp-relationship (graph shin &optional (ra-list nil))
  (assert-graph graph)
  (labels ((core (graph shin ra-list)
             (when ra-list
               (let ((ra (car ra-list))
                     (id (%id shin)))
                 (if (or (gethash id (get-root-object graph (up::get-objects-slot-index-name ra 'from-id)))
                         (gethash id (get-root-object graph (up::get-objects-slot-index-name ra 'to-id))))
                     t
                     (core graph shin (cdr ra-list)))))))
    (core graph shin ra-list)))

(defun assert-not-exist-edge (graph vertex)
  (assert (not (existp-relationship graph vertex))))
