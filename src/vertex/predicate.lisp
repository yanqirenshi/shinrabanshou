(in-package :shinrabanshou)

(defgeneric vertexp (obj)
  (:documentation "symbolで指定された class が vertex のサブクラスかどうかを返す。")
  (:method (obj) (declare (ignore obj)) nil)
  (:method ((vertex shin)) t)
  (:method ((class-symbol symbol))
    (handler-case
        (vertexp (make-instance class-symbol))
      (error () nil))))
