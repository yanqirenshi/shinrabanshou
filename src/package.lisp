(in-package :cl-user)
(defpackage shinrabanshou
  (:nicknames :shinra)
  (:use #:cl
        #:alexandria
        #:cl-ppcre
        #:upanishad
        #:shinrabanshou.conditions
        #:shinrabanshou.utility)
  (:export #:%id)
  (:export #:banshou)
  (:export #:shin)
  (:export #:ra
           #:from-id
           #:from-class
           #:to-id
           #:to-class
           #:edge-type)
  ;; banshou
  (:export #:make-banshou)
  ;; paradicate
  (:export #:existp)
  ;; vertex
  (:export #:vertexp
           #:find-vertex
           #:get-vertex
           #:tx-create-vertex
           #:tx-update-vertex
           #:tx-delete-vertex)
  ;; edge
  (:export #:edgep
           #:get-from-vertex
           #:get-to-vertex
           #:tx-create-edge
           #:tx-delete-edge
           #:tx-change-vertex
           #:tx-change-type)
  ;; relation
  (:export #:find-r
           #:find-r-edge
           #:find-r-vertex
           #:get-r
           #:get-r-edge
           #:get-r-vertex))
(in-package :shinrabanshou)
