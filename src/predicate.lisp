(in-package :shinrabanshou)

(defgeneric existp (graph rsc)
  (:documentation "graph に rsc が存在するかを返します。")
  (:method ((graph banshou) (vertex shin))
    (assert-graph graph)
    (not (null (up:get-meme graph
                            (class-name (class-of vertex))
                            :%id (%id vertex)))))
  (:method ((graph banshou) (edge ra))
    (assert-graph graph)
    (let ((exist (get-object-at-%id graph (class-name (class-of edge)) (%id edge))))
      (when (not (null exist))
        (if
         ;; これ以降は不要なチェックみたいになっとるけど。。。。
         ;; これ以降のチェックがNGの場合は 例外発生させてもエエレベルなんよね。やっぱそうしよ。
         (and (=  (from-id    edge) (from-id    exist))
              (=  (to-id      edge) (to-id      exist))
              (eq (from-class edge) (from-class exist))
              (eq (to-class   edge) (to-class   exist))
              (eq (edge-type  edge) (edge-type  exist)))
         t
         (error "~a" (%id edge)))))))
