(in-package :cl-user)
(defpackage shinrabanshou.utility
  (:nicknames :shinra.utility)
  (:use #:cl)
  (:export #:trim-string
           #:get-class))
(in-package :shinrabanshou.utility)

;; #\U+3000 : 全角スペース
;; #\Return : ^M
;; #\U+FEFF : なんだったっけ？ sbcl では NG?
(defun trim-string (seq &key (char-bag '(#\Space #\Tab #\Newline #\Return)))
  "cl:string-trim のラッパーです。
char-bag を指定するのが面倒なのでデフォルトでセットするようにしました。
あと cl:string-trim は nil を入力にすると \"NIL\" を返すので nil を返すようにしています。"
  (when seq
    (string-trim char-bag seq)))

(defun get-class (obj)
 (class-name (class-of obj)))
