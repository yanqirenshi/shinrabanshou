(in-package :shinrabanshou.persistence)

(defun write-xml-tag-ra-slot (stream ra slot state)
  (let ((slot-value (slot-value ra slot)))
    (write-xml-tag-slot-befor stream slot)
    (upanishad.persistence::serialize-xml-internal slot-value stream state)
    (write-xml-tag-slot-after stream)))

;; (defun write-xml-tag-ra-slots (stream ra serialization-state)
;;   (loop :for slot :in (get-serializable-slots serialization-state ra)
;;         :do (when (slot-boundp ra slot)
;;               (write-xml-tag-ra-slot stream ra slot serialization-state))))

(defun write-xml-tag-ra-slots (stream ra state slots)
  (alexandria:when-let ((slot (car slots)))
    (print slot)
    (when (slot-boundp ra slot)
      (write-xml-tag-ra-slot stream ra slot state))
    (write-xml-tag-ra-slots stream ra state (cdr slots))))

(defun write-xml-tag-ra (stream class ra state)
  (let ((id (set-known-object state ra))
        (slots (get-serializable-slots state ra)))
    (write-xml-tag-object-befor stream id class)
    (write-xml-tag-ra-slots     stream ra state slots)
    (write-xml-tag-object-after stream)))

(defmethod upanishad.persistence::serialize-xml-internal ((ra shinrabanshou::ra) stream state)
  (let ((id (known-object-id state ra))
        (class (class-name (class-of ra))))
    (if id
        (write-xml-tag-ref stream class id)
        (write-xml-tag-ra  stream class ra state))))
