(in-package :shinrabanshou.persistence)

;;;;;
;;;;; Ref tag
;;;;;
(defun write-xml-tag-ref (stream class id)
  (write-string "<REF ID=\"" stream)
  (prin1 id stream)
  (write-string "\" CLASS=\"" stream)
  (print-symbol-xml class stream)
  (write-string "\"/>" stream))

;;;;;
;;;;; Object Slot tag
;;;;;
(defun write-xml-tag-slot-befor (stream slot)
  (write-string "<SLOT NAME=\"" stream)
  (print-symbol-xml slot stream)
  (write-string "\">" stream))

(defun write-xml-tag-slot-after (stream)
  (write-string "</SLOT>" stream))

;;;;;
;;;;; Object tag
;;;;;
(defun write-xml-tag-object-befor (stream id class)
  (write-string "<OBJECT ID=\"" stream)
  (prin1 id stream)
  (write-string "\" CLASS=\"" stream)
  (print-symbol-xml class stream)
  (princ "\">" stream))

(defun write-xml-tag-object-after (stream)
  (write-string "</OBJECT>" stream))
