(in-package :cl-user)
(defpackage shinrabanshou.persistence
  (:nicknames :shinra.persistence)
  (:import-from :upanishad.pool
                #:snapshot
                #:restore
                #:execute-transaction)
  (:import-from :upanishad.persistence
                #:known-object-id
                #:set-known-object
                #:print-symbol-xml
                #:get-serializable-slots)
  (:export #:snapshot
           #:restore
           #:execute-transaction)
  (:use #:cl))
(in-package :shinrabanshou.persistence)
