(in-package :shinrabanshou)

;;;;;
;;;;; find-r-edge
;;;;;
(defgeneric find-r-edge (graph edge-class-symbol start vertex &key edge-type vertex-class)
  (:documentation ""))


;;;;;
;;;;; find-r
;;;;;
(defun find-r-get-vertex (graph edge start)
  (let ((start-symbol (cond ((eq start :from) '(to-class   to-id))
                            ((eq start :to  ) '(from-class from-id)))))
    (up:get-meme graph
                 (funcall (first  start-symbol) edge)
                 :%id (funcall (second start-symbol) edge))))


(defgeneric find-r (graph edge-class-symbol start vertex &key edge-type vertex-class)
  (:documentation "")
  (:method ((graph banshou)
            (edge-class-symbol symbol)
            start
            (vertex shin)
            &key edge-type vertex-class)
    (mapcar #'(lambda (edge)
                (list :edge edge
                      :vertex (find-r-get-vertex graph edge start)))
            (find-r-edge graph edge-class-symbol
                         start vertex
                         :edge-type edge-type :vertex-class vertex-class))))


;;;;;
;;;;; find-r-vertex
;;;;;
(defgeneric find-r-vertex (graph edge-class-symbol start vertex &key edge-type vertex-class)
  (:documentation "")
  (:method ((graph banshou)
            (edge-class-symbol symbol)
            start
            (vertex shin)
            &key edge-type vertex-class)
    (mapcar #'(lambda (data)
                (getf data :vertex))
            (find-r graph edge-class-symbol
                    start vertex
                    :edge-type edge-type :vertex-class vertex-class))))
