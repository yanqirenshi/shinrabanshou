(in-package :shinrabanshou)

(defgeneric get-r (graph edge-class-symbol start start-vertex end-vertex rtype)
  (:documentation "")
  (:method ((graph banshou) (edge-class-symbol symbol)
            start
            (start-vertex shin) (end-vertex shin) rtype)
    (first
     (remove-if #'(lambda (r)
                    (let ((vertex (getf r :vertex))
                          (edge   (getf r :edge)))
                      (not (and (= (%id end-vertex)
                                   (%id vertex))
                                (eq rtype (edge-type edge))))))
                (find-r graph edge-class-symbol start start-vertex)))))


(defgeneric get-r-edge (graph edge-class-symbol start start-vertex end-vertex rtype)
  (:documentation "")
  (:method ((graph banshou) (edge-class-symbol symbol)
            start
            (start-vertex shin) (end-vertex shin) rtype)
    (let ((r (get-r graph edge-class-symbol start start-vertex end-vertex rtype)))
      (when r (getf r :edge)))))


(defgeneric get-r-vertex (graph edge-class-symbol start start-vertex end-vertex rtype)
  (:documentation "")
  (:method ((graph banshou) (edge-class-symbol symbol)
            start
            (start-vertex shin) (end-vertex shin) rtype)
    (let ((r (get-r graph edge-class-symbol start start-vertex end-vertex rtype)))
      (when r (getf r :vertex)))))
