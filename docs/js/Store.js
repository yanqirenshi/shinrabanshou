class Store extends Vanilla_Redux_Store {
    constructor(reducer) {
        super(reducer, Immutable.Map({}));
    }
    pages () {
        return [
            {
                code: "home", title: "",
                menu_label: '家',
                active_section: 'root',
                home_section: 'root',
                sections: [
                    { code: 'root', tag: 'home-sec_root' }
                ],
                stye: {
                    color: { 1: '#fdeff2', 2: '#e0e0e0', 3: '#e198b4', 4: '#ffffff', 5: '#eeeeee', 5: '#333333' }
                }
            },
            {
                code: "shin", title: "",
                menu_label: '森',
                active_section: 'root',
                home_section: 'root',
                sections: [
                    { code: 'root',             tag: 'shin-sec_root' },
                    { code: 'find-vertex',      tag: 'shinrabanshou_find-vertex' },
                    { code: 'get-vertex',       tag: 'shinrabanshou_get-vertex' },
                    { code: 'tx-create-vertex', tag: 'shinrabanshou_tx-create-vertex' },
                    { code: 'tx-update-vertex', tag: 'shinrabanshou_tx-update-vertex' },
                    { code: 'tx-delete-vertex', tag: 'shinrabanshou_tx-delete-vertex' },
                ],
                stye: {
                    color: { 1: '#fdeff2', 2: '#e0e0e0', 3: '#e198b4', 4: '#ffffff', 5: '#eeeeee', 5: '#333333' }
                }
            },
            {
                code: "ra", title: "",
                menu_label: '羅',
                active_section: 'root',
                home_section: 'root',
                sections: [
                    { code: 'root',           tag: 'ra-sec_root' },
                    { code: 'find-edge',      tag: 'shinrabanshou_find-edge' },
                    { code: 'get-edge',       tag: 'shinrabanshou_get-edge' },
                    { code: 'tx-create-edge', tag: 'shinrabanshou_tx-create-edge' },
                    { code: 'tx-delete-edge', tag: 'shinrabanshou_tx-delete-edge' },
                ],
                stye: {
                    color: { 1: '#fdeff2', 2: '#e0e0e0', 3: '#e198b4', 4: '#ffffff', 5: '#eeeeee', 5: '#333333' }
                }
            },
            {
                code: "banshou", title: "",
                menu_label: '万象',
                active_section: 'root',
                home_section: 'root',
                sections: [
                    { code: 'root',          tag: 'banshou-sec_root' },
                    { code: 'vertexes',      tag: 'shinrabanshou_slot_vertexes' },
                    { code: 'edges',         tag: 'shinrabanshou_slot_edges' },
                    { code: 'init-vertexes', tag: 'shinrabanshou_init-vertexes' },
                    { code: 'init-edges',    tag: 'shinrabanshou_init-edges' },
                ],
                stye: {
                    color: { 1: '#fdeff2', 2: '#e0e0e0', 3: '#e198b4', 4: '#ffffff', 5: '#eeeeee', 5: '#333333' }
                }
            },
            {
                code: "index", title: "",
                menu_label: '索引',
                active_section: 'root',
                home_section: 'root',
                sections: [
                    { code: 'root', tag: 'index-sec_root' },
                ],
                stye: {
                    color: { 1: '#fdeff2', 2: '#e0e0e0', 3: '#e198b4', 4: '#ffffff', 5: '#eeeeee', 5: '#333333' }
                }
            },
            {
                code: "query", title: "",
                menu_label: 'Q',
                active_section: 'root',
                home_section: 'root',
                sections: [
                    { code: 'root', tag: 'query-sec_root' },
                    { code: 'q',    tag: 'shinrabanshou_q' },
                ],
                stye: {
                    color: { 1: '#fdeff2', 2: '#e0e0e0', 3: '#e198b4', 4: '#ffffff', 5: '#eeeeee', 5: '#333333' }
                }
            },
            {
                code: "persistence", title: "",
                menu_label: '永',
                active_section: 'root',
                home_section: 'root',
                sections: [
                    { code: 'root', tag: 'persistence-sec_root' },
                ],
                stye: {
                    color: { 1: '#fdeff2', 2: '#e0e0e0', 3: '#e198b4', 4: '#ffffff', 5: '#eeeeee', 5: '#333333' }
                }
            },
            {
                code: "graph-theory", title: "",
                menu_label: '論',
                active_section: 'root',
                home_section: 'root',
                sections: [
                    { code: 'root', tag: 'graph-theory-sec_root' },
                ],
                stye: {
                    color: { 1: '#fdeff2', 2: '#e0e0e0', 3: '#e198b4', 4: '#ffffff', 5: '#eeeeee', 5: '#333333' }
                }
            },
        ];
    }
    operators () {
        return [
            {
                category: 'P:述語',
                href: null,
                class:'',
                type: '',
                symbol: 'EDGEP',
                package: 'SHINRABANSHOU',
                description: ''
            },
            {
                category: 'C:作成',
                href: '#ra/tx-create-edge',
                class:'',
                type: '',
                symbol: 'TX-CREATE-EDGE',
                package: 'SHINRABANSHOU',
                description: ''
            },
            {
                category: 'R:検索',
                href: '#ra/find-edge',
                class:'',
                type: '',
                symbol: 'FIND-EDGE',
                package: 'SHINRABANSHOU',
                description: ''
            },
            {
                category: 'R:取得',
                href: '#ra/get-edge',
                class:'',
                type: '',
                symbol: 'GET-EDGE',
                package: 'SHINRABANSHOU',
                description: ''
            },
            {
                category: 'U:更新',
                href: null,
                class:'',
                type: '',
                symbol: '---',
                package: 'SHINRABANSHOU',
                description: ''
            },
            {
                category: 'D:削除',
                href: '#ra/tx-delete-edgexsg',
                class:'',
                type: '',
                symbol: 'TX-DELETE-EDGE',
                package: 'SHINRABANSHOU',
                description: ''
            },
            {
                category: 'R:取得',
                href: null,
                class:'',
                type: '',
                symbol: 'GET-FROM-VERTEX',
                package: 'SHINRABANSHOU',
                description: ''
            },
            {
                category: 'R:取得',
                href: null,
                class:'',
                type: '',
                symbol: 'GET-TO-VERTEX',
                package: 'SHINRABANSHOU',
                description: ''
            },
            {
                category: 'U:更新',
                href: null,
                class:'',
                type: '',
                symbol: 'TX-CHANGE-VERTEX',
                package: 'SHINRABANSHOU',
                description: ''
            },
            {
                category: 'U:更新',
                href: null,
                class:'',
                type: '',
                symbol: 'TX-CHANGE-TYPE',
                package: 'SHINRABANSHOU',
                description: ''
            },
            {
                category: 'P:述語',
                href: '#shin/vertexp',
                class:'',
                type: '',
                symbol: 'VERTEXP',
                package: 'SHINRABANSHOU',
                description: ''
            },
            {
                category: 'C:作成',
                href: '#shin/tx-create-vertex',
                class:'',
                type: '',
                symbol: 'TX-CREATE-VERTEX',
                package: 'SHINRABANSHOU',
                description: ''
            },
            {
                category: 'R:検索',
                href: '#shin/find-vertex',
                class:'',
                type: '',
                symbol: 'FIND-VERTEX',
                package: 'SHINRABANSHOU',
                description: ''
            },
            {
                category: 'R:取得',
                href: '#shin/get-vertex',
                class:'',
                type: '',
                symbol: 'GET-VERTEX',
                package: 'SHINRABANSHOU',
                description: ''
            },
            {
                category: 'U:更新',
                href: '#shin/tx-update-vertex',
                class:'',
                type: '',
                symbol: 'TX-UPDATE-VERTEX',
                package: 'SHINRABANSHOU',
                description: ''
            },
            {
                category: 'D:削除',
                href: '#shin/tx-delete-vertex',
                class:'',
                type: '',
                symbol: 'TX-DELETE-VERTEX',
                package: 'SHINRABANSHOU',
                description: ''
            },
            {
                category: 'C:作成',
                href: null,
                class:'',
                type: '',
                symbol: 'MAKE-BANSHOU',
                package: 'SHINRABANSHOU',
                description: ''
            },
            {
                category: '???',
                href: null,
                class:'',
                type: '',
                symbol: 'STATISTICS-VERTEXES',
                package: 'SHINRABANSHOU',
                description: 'vertex のクラス一覧を取得します。'
            },
            {
                category: '???',
                href: null,
                class:'',
                type: '',
                symbol: 'STATISTICS-EDGES',
                package: 'SHINRABANSHOU',
                description: 'edge のクラス一覧を取得します。'
            },
            {
                category: '???',
                href: null,
                class:'',
                type: '',
                symbol: 'STATISTICS-INDEXS',
                package: 'SHINRABANSHOU',
                description:  'edge のクラス一覧を取得します。'
            },
        ];
    }
    init () {
        let data = {
            site: {
                active_page: 'home',
                home_page: 'home',
                pages: this.pages(),
            },
            operators: this.operators()
        };

        for (var i in data.site.pages) {
            let page = data.site.pages[i];
            for (var k in page.sections) {
                let section = page.sections[k];
                let hash = '#' + page.code;

                if (section.code!='root')
                    hash += '/' + section.code;

                section.hash = hash;
            }
        }


        this._contents = Immutable.Map(data);
        return this;
    }
}
