riot.tag2('app', '<menu-bar brand="{{label:\'RT\'}}" site="{site()}" moves="{[]}"></menu-bar> <div ref="page-area" style="margin-left:55px;"></div>', 'app > .page { width: 100vw; overflow: hidden; display: block; } app .hide,[data-is="app"] .hide{ display: none; }', '', function(opts) {
     this.site = () => {
         return STORE.state().get('site');
     };

     STORE.subscribe((action)=>{
         if (action.type!='MOVE-PAGE')
             return;

         let tags= this.tags;

         tags['menu-bar'].update();
         ROUTER.switchPage(this, this.refs['page-area'], this.site());
     })

     window.addEventListener('resize', (event) => {
         this.update();
     });

     if (location.hash=='')
         location.hash='#home'
});

riot.tag2('banshou-sec_root', '<section-2 title="Class: BANSHOU"> </section-2> <shinrabanshou_class_banshou></shinrabanshou_class_banshou> <section-footer></section-footer>', '', '', function(opts) {
});

riot.tag2('banshou', '', '', '', function(opts) {
     this.mixin(MIXINS.page);

     this.on('mount', () => { this.draw(); });
     this.on('update', () => { this.draw(); });
});

riot.tag2('shinrabanshou_class_banshou', '<section class="section" style="padding-top:11px;"> <div class="container"> <page-tabs core="{page_tabs}" callback="{clickTab}"></page-tabs> <div style="margin-top:33px;"> <shinrabanshou_class_banshou_readme class="hide"></shinrabanshou_class_banshou_readme> <shinrabanshou_class_banshou_operators class="hide"></shinrabanshou_class_banshou_operators> <shinrabanshou_class_banshou_slots class="hide"></shinrabanshou_class_banshou_slots> </div> </div> </section>', '', '', function(opts) {
     this.page_tabs = new PageTabs([
         {code: 'readme',   label: 'README',   tag: 'shinrabanshou_class_banshou_readme' },
         {code: 'slots',    label: 'SLOTS',    tag: 'shinrabanshou_class_banshou_slots' },
         {code: 'oprators', label: 'OPRATORS', tag: 'shinrabanshou_class_banshou_operators' },
     ]);

     this.on('mount', () => {
         this.page_tabs.switchTab(this.tags)
         this.update();
     });

     this.clickTab = (e, action, data) => {
         if (this.page_tabs.switchTab(this.tags, data.code))
             this.update();
     };
});

riot.tag2('shinrabanshou_class_banshou_operators', '<section-3 title="Operators:" data="{data}"> <section-4 title="Basic" data="{opts.data}"> <div class="contents"> <p>羅(Edge)の基本操作</p> <operator-list data="{opts.data.operators.basic}"></operator-list> </div> </section-4> <section-4 title="Statistics" data="{opts.data}"> <div class="contents"> <operator-list data="{opts.data.operators.statistics}"></operator-list> </div> </section-4> </section-3> <section-footer></section-footer>', '', '', function(opts) {
     this.operators_basic = [
         { type: 'C:作成', href: null, symbol: 'MAKE-BANSHOU',  package: 'SHINRABANSHOU', description: '' },
     ];

     this.operators_statistics = [
         { type: '???', href: null, symbol: 'STATISTICS-VERTEXES', package: 'SHINRABANSHOU', description: 'vertex のクラス一覧を取得します。'  },
         { type: '???', href: null, symbol: 'STATISTICS-EDGES',    package: 'SHINRABANSHOU', description: 'edge のクラス一覧を取得します。'  },
         { type: '???', href: null, symbol: 'STATISTICS-INDEXS',   package: 'SHINRABANSHOU', description:  'edge のクラス一覧を取得します。'  },
     ];

     this.data = {
         operators: {
             basic: this.operators_basic,
             statistics: this.operators_statistics,
         }
     };
});

riot.tag2('shinrabanshou_class_banshou_readme', '<section-3 title="Description:"> </section-3>', '', '', function(opts) {
});

riot.tag2('shinrabanshou_class_banshou_slots', '<section-3 title="Slots:" data="{slots}"> <div class="contents"> <slot-list data="{opts.data}"></slot-list> </div> </section-3>', '', '', function(opts) {
     this.slots = [
         { name: 'VERTEXES',  accessor: 'vertexes',  initarg: '---',  type: 'hash-table', initform: '(make-hash-table :test \'eq)', href: '#banshou/vertexes' },
         { name: 'EDGES',  accessor: 'edges',  initarg: '(make-hash-table :test \'eq)',  type: 'hash-table', initform: '---', href: '#banshou/edges' },
     ];
});

riot.tag2('class-list', '<table class="table is-bordered is-striped is-narrow is-hoverable"> <thead> <tr> <td>package</td> <td>name</td> <td>description</td> </tr> </thead> <tbody> <tr each="{opts.data}"> <td>{package}</td> <td>{name}</td> <td>{description}</td> </tr> </tbody> </table>', '', '', function(opts) {
});

riot.tag2('force', '<section class="section"> <div class="container"> <h1 class="title">Class: FORCE</h1> <h2 class="subtitle"> </h2> </div> </section>', '', '', function(opts) {
});

riot.tag2('naming', '<section class="section"> <div class="container"> <h1 class="title">Class: NAMING</h1> <h2 class="subtitle"> </h2> </div> </section>', '', '', function(opts) {
});

riot.tag2('shinrabanshou_class_ra', '<section class="section" style="padding-top:11px;"> <div class="container"> <page-tabs core="{page_tabs}" callback="{clickTab}"></page-tabs> <div style="margin-top:33px;"> <shinrabanshou_class_ra_readme class="hide"></shinrabanshou_class_ra_readme> <shinrabanshou_class_ra_operators class="hide"></shinrabanshou_class_ra_operators> <shinrabanshou_class_ra_slots class="hide"></shinrabanshou_class_ra_slots> </div> </div> </section>', '', '', function(opts) {
     this.page_tabs = new PageTabs([
         {code: 'readme',   label: 'README',   tag: 'shinrabanshou_class_ra_readme' },
         {code: 'slots',    label: 'SLOTS',    tag: 'shinrabanshou_class_ra_slots' },
         {code: 'oprators', label: 'OPRATORS', tag: 'shinrabanshou_class_ra_operators' },
     ]);

     this.on('mount', () => {
         this.page_tabs.switchTab(this.tags)
         this.update();
     });

     this.clickTab = (e, action, data) => {
         if (this.page_tabs.switchTab(this.tags, data.code))
             this.update();
     };
});

riot.tag2('shinrabanshou_class_ra_operators', '<section-3 title="Operators:" data="{data}"> <section-4 title="Basic" data="{opts.data}"> <div class="contents"> <p>羅(Edge)の基本操作</p> <operator-list data="{opts.data.operators.basic}"></operator-list> </div> </section-4> <section-4 title="Slots" data="{opts.data}"> <div class="contents"> <operator-list data="{opts.data.operators.slots}"></operator-list> </div> </section-4> </section-3>', '', '', function(opts) {
     this.operators_basic = [
         { type: 'P:述語', href: null,                    symbol: 'EDGEP',          package: 'SHINRABANSHOU', description: '' },
         { type: 'C:作成', href: '#ra/tx-create-edge',    symbol: 'TX-CREATE-EDGE', package: 'SHINRABANSHOU', description: '' },
         { type: 'R:検索', href: '#ra/find-edge',         symbol: 'FIND-EDGE',      package: 'SHINRABANSHOU', description: '' },
         { type: 'R:取得', href: '#ra/get-edge',          symbol: 'GET-EDGE',       package: 'SHINRABANSHOU', description: '' },
         { type: 'U:更新', href: null,                    symbol: '---',            package: 'SHINRABANSHOU', description: '' },
         { type: 'D:削除', href: '#ra/tx-delete-edgexsg', symbol: 'TX-DELETE-EDGE', package: 'SHINRABANSHOU', description: '' },
     ];

     this.operators_slots = [
         { type: 'R:取得', href: null, symbol: 'GET-FROM-VERTEX',  package: 'SHINRABANSHOU', description: '' },
         { type: 'R:取得', href: null, symbol: 'GET-TO-VERTEX',    package: 'SHINRABANSHOU', description: '' },
         { type: 'U:更新', href: null, symbol: 'TX-CHANGE-VERTEX', package: 'SHINRABANSHOU', description: '' },
         { type: 'U:更新', href: null, symbol: 'TX-CHANGE-TYPE',   package: 'SHINRABANSHOU', description: '' },
     ];

     this.data = {
         operators: {
             basic: this.operators_basic,
             slots: this.operators_slots,
         }
     };
});

riot.tag2('shinrabanshou_class_ra_readme', '<section-3 title="Description:"> <div class="contents"> <p>Edge を表すクラス</p> </div> </section-3>', '', '', function(opts) {
});

riot.tag2('shinrabanshou_class_ra_slots', '<section-3 title="Slots:" data="{slots}"> <div class="contents"> <slot-list data="{opts.data}"></slot-list> </div> </section-3>', '', '', function(opts) {
     this.slots = [
         { name: 'EDGE-TYPE',  accessor: 'edge-type',  initarg: 'edge-type',  type: 'symbol (keyword)', initform: 'nil', href: '#ra/edge-type' },
         { name: 'FROM',       accessor: 'from',       initarg: 'from',       type: 'shin',             initform: 'nil', href: '#ra/from' },
         { name: 'FROM-ID',    accessor: 'from-id',    initarg: 'from-id',    type: 'Integer',          initform: 'nil', },
         { name: 'FROM-CLASS', accessor: 'from-class', initarg: 'from-class', type: 'Symbol (class)',   initform: 'nil', },
         { name: 'TO',         accessor: 'to',         initarg: 'to',         type: 'shin',             initform: 'nil', href: '#ra/to' },
         { name: 'TO-ID',      accessor: 'to-id',      initarg: 'to-id',      type: 'Integer',          initform: 'nil', },
         { name: 'TO-CLASS',   accessor: 'to-class',   initarg: 'to-class',   type: 'Symbol (class)',   initform: 'nil', },
     ];
});

riot.tag2('shinrabanshou_class_shin', '<section class="section" style="padding-top:11px;"> <div class="container"> <page-tabs core="{page_tabs}" callback="{clickTab}"></page-tabs> <div style="margin-top:33px;"> <shinrabanshou_class_shin_readme class="hide"></shinrabanshou_class_shin_readme> <shinrabanshou_class_shin_operators class="hide"></shinrabanshou_class_shin_operators> <shinrabanshou_class_shin_slots class="hide"></shinrabanshou_class_shin_slots> </div> </div> </section>', '', '', function(opts) {
     this.page_tabs = new PageTabs([
         {code: 'readme',   label: 'README',   tag: 'shinrabanshou_class_shin_readme' },
         {code: 'slots',    label: 'SLOTS',    tag: 'shinrabanshou_class_shin_slots' },
         {code: 'oprators', label: 'OPRATORS', tag: 'shinrabanshou_class_shin_operators' },
     ]);

     this.on('mount', () => {
         this.page_tabs.switchTab(this.tags)
         this.update();
     });

     this.clickTab = (e, action, data) => {
         if (this.page_tabs.switchTab(this.tags, data.code))
             this.update();
     };
});

riot.tag2('shinrabanshou_class_shin_operators', '<section-3 title="Operators:" operators="{operators()}"> <div class="contents"> <p>森(Vertex)の操作</p> <operator-list data="{opts.operators}"></operator-list> </div> </section-3> <section-footer></section-footer>', '', '', function(opts) {
     this.operators = () => {
         return [
             { type: 'P:述語', href: '#shin/vertexp',          symbol: 'VERTEXP',          package: 'SHINRABANSHOU', description: '' },
             { type: 'C:作成', href: '#shin/tx-create-vertex', symbol: 'TX-CREATE-VERTEX', package: 'SHINRABANSHOU', description: '' },
             { type: 'R:検索', href: '#shin/find-vertex',      symbol: 'FIND-VERTEX',      package: 'SHINRABANSHOU', description: '' },
             { type: 'R:取得', href: '#shin/get-vertex',       symbol: 'GET-VERTEX',       package: 'SHINRABANSHOU', description: '' },
             { type: 'U:更新', href: '#shin/tx-update-vertex', symbol: 'TX-UPDATE-VERTEX', package: 'SHINRABANSHOU', description: '' },
             { type: 'D:削除', href: '#shin/tx-delete-vertex', symbol: 'TX-DELETE-VERTEX', package: 'SHINRABANSHOU', description: '' },
         ];
     };
});

riot.tag2('shinrabanshou_class_shin_readme', '<section-3 title="Description:"> <div class="contents"><p>Vertex を表わすクラス</p></div> </section-3>', '', '', function(opts) {
});

riot.tag2('shinrabanshou_class_shin_slots', '<section-3 title="Slots:"> <div class="contents"><p>固有のスロットはありません。</p></div> </section-3>', '', '', function(opts) {
});

riot.tag2('user', '<section class="section"> <div class="container"> <h1 class="title">Class: USER</h1> <h2 class="subtitle"> </h2> </div> </section>', '', '', function(opts) {
});

riot.tag2('menu-bar', '<aside class="menu"> <p ref="brand" class="menu-label" onclick="{clickBrand}"> {opts.brand.label} </p> <ul class="menu-list"> <li each="{opts.site.pages}"> <a class="{opts.site.active_page==code ? \'is-active\' : \'\'}" href="{\'#\' + code}"> {menu_label} </a> </li> </ul> </aside> <div class="move-page-menu hide" ref="move-panel"> <p each="{moves()}"> <a href="{href}">{label}</a> </p> </div>', 'menu-bar .move-page-menu { z-index: 666665; background: #ffffff; position: fixed; left: 55px; top: 0px; min-width: 111px; height: 100vh; box-shadow: 2px 0px 8px 0px #e0e0e0; padding: 22px 55px 22px 22px; } menu-bar .move-page-menu.hide { display: none; } menu-bar .move-page-menu > p { margin-bottom: 11px; } menu-bar > .menu { z-index: 666666; height: 100vh; width: 55px; padding: 11px 0px 11px 11px; position: fixed; left: 0px; top: 0px; background: #233D57; } menu-bar .menu-list a:hover { color: #BC361D; } menu-bar .menu-label, menu-bar .menu-list a { padding: 0; width: 33px; height: 33px; text-align: center; margin-top: 8px; border-radius: 3px; background: none; color: #ffffff; font-weight: bold; padding-top: 7px; font-size: 14px; } menu-bar .menu-label,[data-is="menu-bar"] .menu-label{ background: #ffffff; color: #BC361D; } menu-bar .menu-label.open,[data-is="menu-bar"] .menu-label.open{ background: #ffffff; color: #BC361D; width: 44px; border-radius: 3px 0px 0px 3px; text-shadow: 0px 0px 1px #eee; padding-right: 11px; } menu-bar .menu-list a.is-active { width: 33px; border-radius: 3px; background: #ffffff; color: #BC361D; text-shadow: 0px 0px 8px #fff; font-weight: bold; }', '', function(opts) {
     this.moves = () => {
         let moves = [];
         return moves.filter((d)=>{
             return d.code != this.opts.current;
         });
     };

     this.brandStatus = (status) => {
         let brand = this.refs['brand'];
         let classes = brand.getAttribute('class').trim().split(' ');

         if (status=='open') {
             if (classes.find((d)=>{ return d!='open'; }))
                 classes.push('open')
         } else {
             if (classes.find((d)=>{ return d=='open'; }))
                 classes = classes.filter((d)=>{ return d!='open'; });
         }
         brand.setAttribute('class', classes.join(' '));
     }

     this.clickBrand = () => {
         let panel = this.refs['move-panel'];
         let classes = panel.getAttribute('class').trim().split(' ');

         if (classes.find((d)=>{ return d=='hide'; })) {
             classes = classes.filter((d)=>{ return d!='hide'; });
             this.brandStatus('open');
         } else {
             classes.push('hide');
             this.brandStatus('close');
         }
         panel.setAttribute('class', classes.join(' '));
     };
});

riot.tag2('page-tabs', '<div class="tabs is-boxed"> <ul> <li each="{opts.core.tabs}" class="{opts.core.active_tab==code ? \'is-active\' : \'\'}"> <a code="{code}" onclick="{clickTab}">{label}</a> </li> </ul> </div>', 'page-tabs li:first-child { margin-left: 55px; }', '', function(opts) {
     this.clickTab = (e) => {
         let code = e.target.getAttribute('code');
         this.opts.callback(e, 'CLICK-TAB', { code: code });
     };
});

riot.tag2('section-2', '<section class="section"> <div class="container"> <h1 class="title is-2">{opts.title}</h1> <h2 class="subtitle">{opts.subtitle}</h2> </div> </section> <div class="black"></div> <div class="orange"></div> <div class="black"></div> <div class="yelow"></div> <div class="black"></div> <div class="red"></div> <div class="black tail"></div> <yield></yield>', 'section-2 > .section { background: #2C3C53; padding: 2rem 1.5rem; } section-2 > .section > .container > .title { color: #D6340E; font-weight: bold; } section-2 .orange { height: 3px; background: #FF691F; width: 100%; } section-2 .yelow { height: 3px; background: #FDF579; width: 100%; } section-2 .red { height: 3px; background: #C22252; width: 100%; } section-2 .black { background: #3B000D; height: 1px; } section-2 .black.tail { margin-bottom: 33px; }', '', function(opts) {
});

riot.tag2('section-3', '<section class="section"> <div class="container"> <h1 class="title is-3">{opts.title}</h1> <yield></yield> </div> </section>', 'section-3 .section { padding-top: 11px; padding-bottom: 33px; } section-3 > .section > .container > .title { color: #BC361D; font-weight: bold; } section-3 .contents { padding-left: 33px; }', '', function(opts) {
});

riot.tag2('section-4', '<section class="section"> <div class="container"> <h1 class="title is-4">{opts.title}</h1> <yield></yield> </div> </section>', 'section-4 .section { padding-top: 11px; padding-bottom: 33px; } section-4 .contents { padding-left: 33px; }', '', function(opts) {
});

riot.tag2('section-5', '<section class="section"> <div class="container"> <h1 class="title is-5">{opts.title}</h1> <yield></yield> </div> </section>', 'section-5 .section { padding-top: 11px; padding-bottom: 33px; } section-5 .contents { padding-left: 33px; }', '', function(opts) {
});

riot.tag2('section-breadcrumb', '<section-container data="{path()}"> <nav class="breadcrumb" aria-label="breadcrumbs"> <ul> <li each="{opts.data}"> <a class="{active ? \'is-active\' : \'\'}" href="{href}" aria-current="page">{label}</a> </li> </ul> </nav> </section-container>', 'section-breadcrumb section-container > .section,[data-is="section-breadcrumb"] section-container > .section{ padding-top: 3px; }', '', function(opts) {
     this.path = () => {
         let hash = location.hash;
         let path = hash.split('/');

         if (path[0] && path[0].substr(0,1)=='#')
             path[0] = path[0].substr(1);

         let out = [];
         let len = path.length;
         let href = null;
         for (var i in path) {
             href = href ? href + '/' + path[i] : '#' + path[i];

             if (i==len-1)
                 out.push({
                     label: path[i],
                     href: hash,
                     active: true
                 });

             else
                 out.push({
                     label: path[i],
                     href: href,
                     active: false
                 });
         }
         return out;
     }
});

riot.tag2('section-container', '<section class="section"> <div class="container"> <h1 class="title is-{opts.no ? opts.no : 3}"> {opts.title} </h1> <h2 class="subtitle">{opts.subtitle}</h2> <yield></yield> </div> </section>', '', '', function(opts) {
});

riot.tag2('section-contents', '<section class="section"> <div class="container"> <h1 class="title is-{opts.no ? opts.no : 3}"> {opts.title} </h1> <h2 class="subtitle">{opts.subtitle}</h2> <div class="contents"> <yield></yield> </div> </div> </section>', 'section-contents > section.section { padding: 0.0rem 1.5rem 2.0rem 1.5rem; }', '', function(opts) {
});

riot.tag2('section-footer', '<footer class="footer"> <div class="container"> <div class="content has-text-centered"> <p> </p> </div> </div> </footer>', 'section-footer > .footer { margin-top: 222px; height: 222px; padding-top: 13px; padding-bottom: 13px; background: #2C3A54; opacity: 0.7; }', '', function(opts) {
});

riot.tag2('section-header-with-breadcrumb', '<section-header title="{opts.title}"></section-header> <section-breadcrumb></section-breadcrumb>', 'section-header-with-breadcrumb section-header > .section,[data-is="section-header-with-breadcrumb"] section-header > .section{ margin-bottom: 3px; }', '', function(opts) {
});

riot.tag2('section-header', '<section class="section"> <div class="container"> <h1 class="title is-{opts.no ? opts.no : 3}"> {opts.title} </h1> <h2 class="subtitle">{opts.subtitle}</h2> <yield></yield> </div> </section>', 'section-header > .section { padding-top: 13px; padding-bottom: 13px; height: 88px; background: #273C57; margin-bottom: 11px; } section-header .title { color: #D6340E; font-weight: bold; } section-header .subtitle { color: #FDFFFE; font-weight: bold; }', '', function(opts) {
});

riot.tag2('section-list', '<table class="table is-bordered is-striped is-narrow is-hoverable"> <thead> <tr> <th>機能</th> <th>概要</th> </tr> </thead> <tbody> <tr each="{data()}"> <td><a href="{hash}">{title}</a></td> <td>{description}</td> </tr> </tbody> </table>', '', '', function(opts) {
     this.data = () => {
         return opts.data.filter((d) => {
             if (d.code=='root') return false;

             let len = d.code.length;
             let suffix = d.code.substr(len-5);
             if (suffix=='_root' || suffix=='-root')
                 return false;

             return true;
         });
     };
});

riot.tag2('class-list', '<table class="table"> <thead> <tr> <td>Package</td> <td>Symbol</td> <td>Description</td> </tr> </thead> <tbody> <tr each="{opts.data}"> <td>{package}</td> <td>{symbol}</td> <td>{description}</td> </tr> </tbody> </table>', '', '', function(opts) {
});

riot.tag2('operator-list', '<table class="table"> <thead> <tr> <td>Package</td> <td>Symbol</td> <td>Type</td> <td>Description</td> </tr> </thead> <tbody> <tr each="{opts.data}"> <td>{package}</td> <td><a href="{href}">{symbol}</a></td> <td>{type}</td> <td>{description}</td> </tr> </tbody> </table>', '', '', function(opts) {
});

riot.tag2('sections-list', '<table class="table"> <tbody> <tr each="{opts.data}"> <td><a href="{hash}">{title}</a></td> </tr> </tbody> </table>', '', '', function(opts) {
});

riot.tag2('slot-list', '<table class="table"> <thead> <tr> <td>Name</td> <td>Accessor</td> <td>Initarg</td> <td>Type</td> <td>Initform</td> </tr> </thead> <tbody> <tr each="{opts.data}"> <td><a href="{href ? href : location.hash}">{name}</a></td> <td>{accessor}</td> <td>{initarg}</td> <td>{type}</td> <td>{initform}</td> </tr> </tbody> </table>', '', '', function(opts) {
});

riot.tag2('graph-theory-section01', '<section-3 title="集合と理論"> </section-3>', '', '', function(opts) {
});

riot.tag2('graph-theory-section02', '<section-3 title="無向グラフと部分グラフ"> </section-3> <section-3 title="部分グラフと連結性"> </section-3> <section-3 title="同型と辺同型"> </section-3> <section-3 title="木と生成木"> </section-3> <section-3 title="生成木の数え上げ"> </section-3> <section-3 title="サイクルとカットセットの基本系統"> </section-3> <section-3 title="2部グラフ"> </section-3>', '', '', function(opts) {
});

riot.tag2('graph-theory-section03', '<section-3 title="スペルナーグラフ"> </section-3> <section-3 title="距離グラフ"> </section-3> <section-3 title="ウェブのリンクグラフ"> </section-3> <section-3 title="ハミルトングラフ"> </section-3> <section-3 title="オイラーグラフ"> </section-3>', '', '', function(opts) {
});

riot.tag2('graph-theory-section04', '<section-3 title="人材割り当て問題"> </section-3> <section-3 title="最適人材割り当て問題"> </section-3>', '', '', function(opts) {
});

riot.tag2('graph-theory-section05', '<section-3 title="ラムゼー数"> </section-3>', '', '', function(opts) {
});

riot.tag2('graph-theory-section06', '<section-3 title="独立集合"> </section-3>', '', '', function(opts) {
});

riot.tag2('graph-theory-section07', '<section-3 title=""> </section-3>', '', '', function(opts) {
});

riot.tag2('graph-theory-section08', '<section-3 title="オイラーの公式"> </section-3> <section-3 title="クラトウスキーの定理"> </section-3> <section-3 title="双対グラフ"> </section-3> <section-3 title="4色問題"> </section-3> <section-3 title="平面グラフの直線描画"> </section-3> <section-3 title="直線描画アルゴリズム"> </section-3> <section-3 title="平面性判定"> </section-3> <section-3 title="3連結判定アルゴリズム"> </section-3> <section-3 title="凸多面体グラフ"> </section-3>', '', '', function(opts) {
});

riot.tag2('graph-theory-sec_root', '<section-2 title="グラフ理論入門" subtitle="平面グラフへの応用" data="{data}"> </section-2> <graph-theory-tabs tabs="{tabs}" active="{active_tab}" callback="{clickTab}"></graph-theory-tabs> <section class="section" style="padding-top: 8px;"> <div class="container"> <graph-theory-section01 type="tab-contents"></graph-theory-section01> <graph-theory-section02 type="tab-contents"></graph-theory-section02> <graph-theory-section03 type="tab-contents"></graph-theory-section03> <graph-theory-section04 type="tab-contents"></graph-theory-section04> <graph-theory-section05 type="tab-contents"></graph-theory-section05> <graph-theory-section06 type="tab-contents"></graph-theory-section06> <graph-theory-section07 type="tab-contents"></graph-theory-section07> <graph-theory-section08 type="tab-contents"></graph-theory-section08> </div> </section> <section-footer></section-footer>', '', '', function(opts) {
     this.active_tab = 'section01';
     this.tabs = [
         { code: 'section01', label: '背理法と帰納法' },
         { code: 'section02', label: 'グラフと部分グラフ' },
         { code: 'section03', label: 'いろいろなグラフ' },
         { code: 'section04', label: 'マッチング' },
         { code: 'section05', label: '辺彩色' },
         { code: 'section06', label: '独立集合と頂点彩色' },
         { code: 'section07', label: '輸送回路網' },
         { code: 'section08', label: '平面グラフ' },
     ];

     this.clickTab = (e) => {
         this.active_tab = e.target.getAttribute('code');
         this.update();
     };

     this.switchTab = () => {
         for (var k in this.tags) {
             tag = this.tags[k];
             if (tag.root.getAttribute('type') != 'tab-contents')
                 continue;
             tag.root.classList.add('hide');
         }

         let code = 'graph-theory-' + this.active_tab;
         this.tags[code].root.classList.remove('hide');
     };
     this.on('update', () => {
         this.switchTab();
     });
     this.on('mount', () => {
         this.switchTab();
     });

});

riot.tag2('graph-theory-tabs', '<section class="section" style="padding-top: 8px; padding-bottom: 8px;"> <div class="container"> <div class="tabs is-boxed"> <ul> <li each="{opts.tabs}" class="{opts.active==code ? \'is-active\' : \'\'}"> <a code="{code}" onclick="{opts.callback}">{label}</a> </li> </ul> </div> </div> </section>', 'graph-theory-tabs li:first-child { margin-left: 22px; }', '', function(opts) {
});

riot.tag2('graph-theory', '', '', '', function(opts) {
     this.mixin(MIXINS.page);

     this.on('mount', () => { this.draw(); });
     this.on('update', () => { this.draw(); });
});

riot.tag2('home-sec_classes', '<section-4 title="List" data="{classes()}"> <div class="contents"> <class-list data="{opts.data}"></class-list> </div> </section-4> <section-4 title="Graph"> <div class="contents"> <class-diagram></class-diagram> </div> </section-4>', '', '', function(opts) {
     this.classes = () => {
         return [
             { symbol: 'SHIN',    package: 'SHINRABANSHOU', description: 'Vertex を表わすクラス'},
             { symbol: 'RA',      package: 'SHINRABANSHOU', description: 'Edge を表わすクラス'},
             { symbol: 'BANSHOU', package: 'SHINRABANSHOU', description: 'Shin と Ra を管理するクラス'},
         ];
     };
});

riot.tag2('home-sec_operators', '<section-4> <div class="contents"> <p> シンプルで規則性のあるオペレータになっています。 </p> <p> オペレータは大きく分類すると以下の四つになります。<br> </p> <div style="margin-left: 33px;"> <ol> <li>CROUD</li> <li>Paradicate</li> <li>Accessor</li> <li>Query</li> </ol> </div> </div> </section-4> <section-4 title="森" data="{shin}"> <div class="contents"> <operator-list data="{opts.data}"></operator-list> </div> </section-4> <section-4 title="羅" data="{ra}"> <div class="contents"> <operator-list data="{opts.data}"></operator-list> </div> </section-4> <section-4 title="万象" data="{banshou}"> <div class="contents"> <operator-list data="{opts.data}"></operator-list> </div> </section-4> <section-4 title="Index" data="{index}"> <div class="contents"> <operator-list data="{opts.data}"></operator-list> </div> </section-4> <section-4 title="Query" data="{query}"> <div class="contents"> <operator-list data="{opts.data}"></operator-list> </div> </section-4>', '', '', function(opts) {
     this.shin = [
         { type: 'P:述語', href: '#home/vertexp',          symbol: 'VERTEXP',          package: 'SHINRABANSHOU', description: '' },
         { type: 'C:作成', href: '#home/tx-create-vertex', symbol: 'TX-CREATE-VERTEX', package: 'SHINRABANSHOU', description: '' },
         { type: 'R:検索', href: '#home/find-vertex',      symbol: 'FIND-VERTEX',      package: 'SHINRABANSHOU', description: '' },
         { type: 'R:取得', href: '#home/get-vertex',       symbol: 'GET-VERTEX',       package: 'SHINRABANSHOU', description: '' },
         { type: 'U:更新', href: '#home/tx-update-vertex', symbol: 'TX-UPDATE-VERTEX', package: 'SHINRABANSHOU', description: '' },
         { type: 'D:削除', href: '#home/tx-delete-vertex', symbol: 'TX-DELETE-VERTEX', package: 'SHINRABANSHOU', description: '' },
     ];

     this.ra = [
         { type: 'P:述語', href: null,                      symbol: 'EDGEP',            package: 'SHINRABANSHOU', description: '' },
         { type: 'C:作成', href: '#home/tx-create-edge',    symbol: 'TX-CREATE-EDGE',   package: 'SHINRABANSHOU', description: '' },
         { type: 'R:検索', href: '#home/find-edge',         symbol: 'FIND-EDGE',        package: 'SHINRABANSHOU', description: '' },
         { type: 'R:取得', href: null,                      symbol: 'GET-FROM-VERTEX',  package: 'SHINRABANSHOU', description: '廃棄しようかな。' },
         { type: 'R:取得', href: null,                      symbol: 'GET-TO-VERTEX',    package: 'SHINRABANSHOU', description: '廃棄しようかな。' },
         { type: 'R:取得', href: '#home/get-edge',          symbol: 'GET-EDGE',         package: 'SHINRABANSHOU', description: '' },
         { type: 'U:更新', href: null,                      symbol: '---',              package: 'SHINRABANSHOU', description: 'Edge は汎用的な update 命令はありません。' },
         { type: 'U:更新', href: null,                      symbol: 'TX-CHANGE-VERTEX', package: 'SHINRABANSHOU', description: '' },
         { type: 'U:更新', href: null,                      symbol: 'TX-CHANGE-TYPE',   package: 'SHINRABANSHOU', description: '' },
         { type: 'D:削除', href: '#home/tx-delete-edgexsg', symbol: 'TX-DELETE-EDGE',   package: 'SHINRABANSHOU', description: '' },
     ];

     this.banshou = [
         { type: 'C:作成', href: null, symbol: 'MAKE-BANSHOU',        package: 'SHINRABANSHOU', description: '' },
         { type: '???',    href: null, symbol: 'STATISTICS-VERTEXES', package: 'SHINRABANSHOU', description: 'vertex のクラス一覧を取得します。'  },
         { type: '???',    href: null, symbol: 'STATISTICS-EDGES',    package: 'SHINRABANSHOU', description: 'edge のクラス一覧を取得します。'  },
         { type: '???',    href: null, symbol: 'STATISTICS-INDEXS',   package: 'SHINRABANSHOU', description:  'edge のクラス一覧を取得します。'  },
     ];

     this.query = [
         { type: 'Q:問合', href: null, symbol: 'Q',             package: 'SHINRABANSHOU', description: '' },
         { type: 'R:検索', href: null, symbol: 'FIND-R',        package: 'SHINRABANSHOU', description: '' },
         { type: 'R:検索', href: null, symbol: 'FIND-R-EDGE',   package: 'SHINRABANSHOU', description: '' },
         { type: 'R:検索', href: null, symbol: 'FIND-R-VERTEX', package: 'SHINRABANSHOU', description: '' },
         { type: 'R:取得', href: null, symbol: 'GET-R',         package: 'SHINRABANSHOU', description: '' },
         { type: 'R:取得', href: null, symbol: 'GET-R-EDGE',    package: 'SHINRABANSHOU', description: '' },
         { type: 'R:取得', href: null, symbol: 'GET-R-VERTEX',  package: 'SHINRABANSHOU', description: '' },
     ];

     this.index = [
         { type: 'C:作成', href: null, symbol: 'TX-CREATE-INDEX', package: 'SHINRABANSHOU', description: '' },
         { type: 'R:取得', href: null, symbol: 'GET-INDEX',       package: 'SHINRABANSHOU', description: '' },
         { type: 'U:更新', href: null, symbol: '---',             package: 'SHINRABANSHOU', description: '' },
         { type: 'D:削除', href: null, symbol: 'TX-DELETE-INDEX', package: 'SHINRABANSHOU', description: '' },
         { type: 'U:更新', href: null, symbol: 'REBUILD-INDEX',   package: 'SHINRABANSHOU', description: '' },
     ];

});

riot.tag2('home-sec_other', '<section-4 title="名前の由来"> <div class="contents"> <p>日本語の森羅万象が由来です。</p> <ol style="margin-left:44px;"> <li>森羅 : 数多く並びつらなること。また，そのもの。</li> <li>万象 : さまざまの形。あらゆる事物。</li> </ol> <p>キルラキルとは同時期に書き初めたので親近感があります。</p> </div> </section-4> <section-4 title="このドキュメントについて"> <div class="contents"> <p>配色はキルラキルから拝借しています。</p> </div> </section-4>', '', '', function(opts) {
});

riot.tag2('home-sec_readme', '<section-3 title="概要"> <div class="contents"> <p>Common Lisp で書かれたグラフデータベースです。</p> <p>グラフは有向グラフです。</p> </div> </section-3> <section-3 title="目指していること"> <div class="contents"> <p>Common Lisp ネイティブでポータブルかつシンプルな GraphDatabase を目指しています。</p> <p>外部のDBを利用すれば良いのでしょうが、インストールしたり設定したりと面倒を感じます。</p> <ol style="margin-left:44px;"> <li>シンプルであること。</li> <li>関係ライブラリを少なくすること。</li> <li>AICD(?) とか難しいことは考えないこと。</li> <li>セットアップにストレスを覚えないこと。</li> <li>Windows環境でも問題なく利用できること。</li> </ol> </div> </section-3> <section-3 title="Author"> <div class="contents"> <p>Satoshi Iwasaki (yanqirenshi@gmail.com)</p> </div> </section-3> <section-3 title="Copyright"> <div class="contents"> <p>Copyright (c) 2014 Satoshi Iwasaki (yanqirenshi@gmail.com)</p> </div> </section-3> <section-3 title="License"> <div class="contents"> <p>Licensed under the LLGPL License.</p> </div> </section-3>', '', '', function(opts) {
});

riot.tag2('home-sec_root', '<section-2 title="森羅万象 (SHINRABANSHOU)" subtitle="Common Lisp で書かれたグラフデータベース"></section-2> <home-tabs tabs="{tabs}" active="{active_tab}" callback="{clickTab}"></home-tabs> <section class="section" style="padding-top: 8px;"> <div class="container"> <home-sec_readme type="tab-contents"></home-sec_readme> <home-sec_classes type="tab-contents"></home-sec_classes> <home-sec_operators type="tab-contents"></home-sec_operators> <home-sec_other type="tab-contents"></home-sec_other> </div> </section> <section-footer type="tab-contents"></section-footer>', '', '', function(opts) {
     this.active_tab = 'readme';
     this.tabs = [
         { code: 'readme',    label: 'README' },
         { code: 'classes',   label: 'Classes' },
         { code: 'operators', label: 'Operators' },
         { code: 'other',     label: '他' },
     ];

     this.clickTab = (e) => {
         this.active_tab = e.target.getAttribute('code');
         this.update();
     };

     this.switchTab = () => {
         for (var k in this.tags) {
             tag = this.tags[k];
             if (tag.root.getAttribute('type') != 'tab-contents')
                 continue;
             tag.root.classList.add('hide');
         }

         let code = 'home-sec_' + this.active_tab;

         this.tags[code].root.classList.remove('hide');
     };
     this.on('update', () => {
         this.switchTab();
     });
     this.on('mount', () => {
         this.switchTab();
     });
});

riot.tag2('home-tabs', '<section class="section" style="padding-top: 8px; padding-bottom: 8px;"> <div class="container"> <div class="tabs is-boxed"> <ul> <li each="{opts.tabs}" class="{opts.active==code ? \'is-active\' : \'\'}"> <a code="{code}" onclick="{opts.callback}">{label}</a> </li> </ul> </div> </div> </section>', 'home-tabs li:first-child { margin-left: 22px; }', '', function(opts) {
});

riot.tag2('home', '', '', '', function(opts) {
     this.mixin(MIXINS.page);

     this.on('mount', () => { this.draw(); });
     this.on('update', () => { this.draw(); });
});

riot.tag2('index-sec_root', '<section-2 title="Index"> <section-3 title="概要"> </section-3> <section-3 title="オペレータ"> <section-4 title="Basic"> <div class="contents"> <table class="table"> <tbody> <tr><td>C:作成</td><th>TX-CREATE-INDEX</th> <td></td></tr> <tr><td>R:取得</td><th>GET-INDEX</th> <td></td></tr> <tr><td>U:更新</td><th>---</th> <td></td></tr> <tr><td>D:削除</td><th>TX-DELETE-INDEX</th> <td></td></tr> </tbody> </table> </div> </section-4> <section-4 title="Maintains"> <div class="contents"> <table class="table"> <tbody> <tr><td>U:更新</td><th>REBUILD-INDEX</th> <td></td></tr> </tbody> </table> </div> </section-4> </section-3> </section-2> <section-footer></section-footer>', '', '', function(opts) {
});

riot.tag2('index', '', '', '', function(opts) {
     this.mixin(MIXINS.page);

     this.on('mount', () => { this.draw(); });
     this.on('update', () => { this.draw(); });
});

riot.tag2('class-diagram', '<pre>作成中。</pre>', '', '', function(opts) {
});

riot.tag2('find-r', '<section class="section"> <div class="container"> <h1 class="title">Function: FIND-R</h1> <h2 class="subtitle"> </h2> </div> </section>', '', '', function(opts) {
});

riot.tag2('operators-matrix1', '<table class="table is-bordered is-striped is-narrow is-hoverable"> <thead> <tr> <th rowspan="2">Target</th> <th colspan="3">Find</th> <th colspan="5">Get</th> </tr> <tr> <th>both</th> <th>vertex</th> <th>edge</th> <th>both</th> <th>vertex</th> <th>edge</th> <th>from-vertex?</th> <th>to-vertex?</th> </tr> </thead> <tbody> <tr> <th>Relationship</th> <td><a href="#operators/find-r">find-r</a></td> <td><a href="#operators/find-r-vertex">find-r-vertex</a></td> <td><a href="#operators/find-r-edge">find-r-edge</a></td> <td><a href="#operators/get-r">get-r</a></td> <td><a href="#operators/get-r-vertex">get-r-vertex</a></td> <td><a href="#operators/get-r-edge">get-r-edge</a></td> <td><a href="#operators/get-from-vertex">get-from-vertex</a></td> <td><a href="#operators/get-to-vertex">get-to-vertex</a></td> </tr> <tr> <th>VERTEX</th> <td>---</td> <td><a href="#operators/find-vertex">find-vertex</a></td> <td>---</td> <td>---</td> <td> <a href="#operators/get-vertex">get-vertex</a> (未実装)<br> <a href="#operators/get-vertex-at">get-vertex-at</a> (廃棄予定) </td> <td>---</td> <td>---</td> <td>---</td> </tr> <tr> <th>EDGE</th> <td>---</td> <td><a href="#operators/find-edge">find-edge</a> (未実装)</td> <td>---</td> <td>---</td> <td><a href="#operators/get-edge">get-edge</a> (未実装)</td> <td>---</td> <td>---</td> <td>---</td> </tr> </tbody> </table>', '', '', function(opts) {
});

riot.tag2('operators-matrix2', '<table class="table is-bordered is-striped is-narrow is-hoverable"> <thead> <tr> <th rowspan="2">Target</th> <th colspan="3">Transaction</th> <th colspan="3">Not Transaction</th> </tr> <tr> <th>Create</th> <th>Update</th> <th>Delete</th> <th>Create</th> <th>Update</th> <th>Delete</th> </tr> </thead> <tbody> <tr> <th>BANSHOU</th> <td>---</td> <td>---</td> <td>---</td> <td><a href="#operators/make-banshou">make-banshou</a></td> <td>---</td> <td>---</td> </tr> <tr> <th>VERTEX</th> <td><a href="#operators/tx-make-vertex">tx-make-vertex</a></td> <td><a href="#operators/tx-change-vertex">tx-change-vertex</a></td> <td><a href="#operators/tx-delete-vertex">tx-delete-vertex</a></td> <td><a href="#operators/make-vertex">make-vertex</a></td> <td><a href="#operators/change-vertex">change-vertex</a> (未実装)</td> <td><a href="#operators/delete-vertex">delete-vertex</a></td> </tr> <tr> <th>EDGE</th> <td><a href="#operators/tx-make-edge">tx-make-edge</a></td> <td> <a href="#operators/tx-change-edge">tx-change-edge</a> (未実装)<br> <a href="#operators/tx-change-type">tx-change-type</a> </td> <td><a href="#operators/tx-delete-edge">tx-delete-edge</a></td> <td><a href="#operators/make-edge">make-edge</a></td> <td><a href="#operators/change-edge">change-edge</a> (未実装)</td> <td><a href="#operators/delete-edge">delete-edge</a></td> </tr> </tbody> </table>', '', '', function(opts) {
});

riot.tag2('operators-matrix3', '<table class="table is-bordered is-striped is-narrow is-hoverable"> <thead> <tr> <th>Target</th> <th>type?</th> <th>exist?</th> </tr> </thead> <tbody> <tr> <th>VERTEX</th> <td><a href="#operators/vertexp">vertexp</a></td> <td rowspan="2"><a href="#operators/existp">existp</a></td> </tr> <tr> <th>EDGE</th> <td><a href="#operators/edgep">edgep</a></td> </tr> <tr> <th>BANSHOU</th> <td><a href="#operators/banshoup">banshoup</a> (未実装)</td> <td>---</td> </tr> </tbody> </table>', '', '', function(opts) {
});

riot.tag2('operators-matrix4', '<table class="table is-bordered is-striped is-narrow is-hoverable"> <thead> <tr> <th>Target</th> <th>%id</th> <th>from-id</th> <th>from-class</th> <th>to-id</th> <th>to-class</th> <th>edge-type</th> </tr> </thead> <tbody> <tr> <th>VERTEX</th> <th><a href="#operators/%id">%id</a></th> <th>---</th> <th>---</th> <th>---</th> <th>---</th> <th>---</th> </tr> <tr> <th>EDGE</th> <td><a href="#operators/%id">%id</a></td> <td><a href="#operators/from-id">from-id</a></td> <td><a href="#operators/from-class">from-class</a></td> <td><a href="#operators/to-id">to-id</a></td> <td><a href="#operators/to-class">to-class</a></td> <td><a href="#operators/edge-type">edge-type</a></td> </tr> </tbody> </table>', '', '', function(opts) {
});

riot.tag2('shinrabanshou_find-edge', '<section-2 title="Generic Function: TX-CREATE-EDGE"> <section-3 title="Syntax"> <div class="context"> <p>find-edge graph class-symbol &key slot value</p> </div> </section-3> <section-3 title="Method Signatures"> <div class="context"> <p>(graph banshou) (class-symbol symbol) &key slot value</p> </div> </section-3> <section-3 title="Arguments and Values"> </section-3> <section-3 title="Description"> </section-3> <section-3 title="Examples"> </section-3> <section-3 title="Affected By"> </section-3> <section-3 title="Exceptional Situations"> </section-3> <section-3 title="See Also"> </section-3> <section-3 title="Notes"> </section-3> </section-2>', '', '', function(opts) {
});

riot.tag2('shinrabanshou_find-vertex', '<section-2 title="Generic Function: FIND-VERTEX"> <section-3 title="Syntax"> <div class="context"> </div> </section-3> <section-3 title="Method Signatures"> <div> </div> </section-3> <section-3 title="Arguments and Values"> </section-3> <section-3 title="Description"> </section-3> <section-3 title="Examples"> </section-3> <section-3 title="Affected By"> </section-3> <section-3 title="Exceptional Situations"> </section-3> <section-3 title="See Also"> </section-3> <section-3 title="Notes"> </section-3> </section-2>', '', '', function(opts) {
});

riot.tag2('shinrabanshou_get-edge', '<section-2 title="Generic Function: TX-CREATE-EDGE"> <section-3 title="Syntax"> <div class="context"> <p>get-edge graph class-symbol &key %id</p> </div> </section-3> <section-3 title="Method Signatures"> <div class="context"> <p>(graph banshou) (class-symbol symbol) &key %id</p> </div> </section-3> <section-3 title="Arguments and Values"> </section-3> <section-3 title="Description"> </section-3> <section-3 title="Examples"> </section-3> <section-3 title="Affected By"> </section-3> <section-3 title="Exceptional Situations"> </section-3> <section-3 title="See Also"> </section-3> <section-3 title="Notes"> </section-3> </section-2>', '', '', function(opts) {
});

riot.tag2('shinrabanshou_get-vertex', '<section-2 title="Generic Function: GET-VERTEX"> <section-3 title="Syntax"> <div class="context"> </div> </section-3> <section-3 title="Method Signatures"> <div> </div> </section-3> <section-3 title="Arguments and Values"> </section-3> <section-3 title="Description"> </section-3> <section-3 title="Examples"> </section-3> <section-3 title="Affected By"> </section-3> <section-3 title="Exceptional Situations"> </section-3> <section-3 title="See Also"> </section-3> <section-3 title="Notes"> </section-3> </section-2>', '', '', function(opts) {
});

riot.tag2('shinrabanshou_q', '<section-2 title="Macro: Q"> <section-3 title="Syntax"> <div class="context"> </div> </section-3> <section-3 title="Method Signatures"> <div class="context"> </div> </section-3> <section-3 title="Arguments and Values"> </section-3> <section-3 title="Description"> </section-3> <section-3 title="Examples"> </section-3> <section-3 title="Affected By"> </section-3> <section-3 title="Exceptional Situations"> </section-3> <section-3 title="See Also"> </section-3> <section-3 title="Notes"> </section-3> </section-2>', '', '', function(opts) {
});

riot.tag2('shinrabanshou_tx-create-edge', '<section-2 title="Generic Function: TX-CREATE-EDGE"> <section-3 title="Syntax"> <div class="contents"> <p>tx-create-edge graph class from to type &optional slots-and-values</p> </div> </section-3> <section-3 title="Method Signatures"> <div class="contents"> <p>(graph banshou) (class symbol) (from shin) (to shin) (type symbol) &optional slots-and-values</p> </div> </section-3> <section-3 title="Arguments and Values"> <section-5 title="graph"></section-5> <section-5 title="class"></section-5> <section-5 title="from"></section-5> <section-5 title="to"></section-5> <section-5 title="tyoe"></section-5> <section-5 title="params"></section-5> </section-3> <section-3 title="Description"> <div class="contents"> <p>RA を作成します。</p> <p>BANSHOU に RA を追加します。</p> <p> BANSHOU に RA の EDGES(MEMES) が存在しない場合、EDGES(MEMES) を自動で追加します。<br> その際に <code>%id</code>、 <code>from.%id</code>、 <code>to.%id</code> のインデックスも作成されます。 </p> </div> </section-3> <section-3 title="Examples"> </section-3> <section-3 title="Affected By"> </section-3> <section-3 title="Exceptional Situations"> </section-3> <section-3 title="See Also"> </section-3> <section-3 title="Notes"> </section-3> </section-2>', '', '', function(opts) {
});

riot.tag2('shinrabanshou_tx-create-vertex', '<section-2 title="Generic Function: TX-CREATE-VERTEX"> <section-3 title="Syntax"> <div class="context"> </div> </section-3> <section-3 title="Method Signatures"> <div> </div> </section-3> <section-3 title="Arguments and Values"> </section-3> <section-3 title="Description"> </section-3> <section-3 title="Examples"> </section-3> <section-3 title="Affected By"> </section-3> <section-3 title="Exceptional Situations"> </section-3> <section-3 title="See Also"> </section-3> <section-3 title="Notes"> </section-3> </section-2>', '', '', function(opts) {
});

riot.tag2('shinrabanshou_tx-delete-edge', '<section-2 title="Generic Function: TX-CREATE-EDGE"> <section-3 title="Syntax"> <div class="context"> <p>tx-delete-edge graph edge)</p> </div> </section-3> <section-3 title="Method Signatures"> <div> <p>(graph banshou) (edge ra)</p> </div> </section-3> <section-3 title="Arguments and Values"> </section-3> <section-3 title="Description"> </section-3> <section-3 title="Examples"> </section-3> <section-3 title="Affected By"> </section-3> <section-3 title="Exceptional Situations"> </section-3> <section-3 title="See Also"> </section-3> <section-3 title="Notes"> </section-3> </section-2>', '', '', function(opts) {
});

riot.tag2('shinrabanshou_tx-delete-vertex', '<section-2 title="Generic Function: TX-DELETE-VERTEX"> <section-3 title="Syntax"> <div class="context"> </div> </section-3> <section-3 title="Method Signatures"> <div> </div> </section-3> <section-3 title="Arguments and Values"> </section-3> <section-3 title="Description"> </section-3> <section-3 title="Examples"> </section-3> <section-3 title="Affected By"> </section-3> <section-3 title="Exceptional Situations"> </section-3> <section-3 title="See Also"> </section-3> <section-3 title="Notes"> </section-3> </section-2>', '', '', function(opts) {
});

riot.tag2('shinrabanshou_tx-update-vertex', '<section-2 title="Generic Function: TX-UPDATE-VERTEX"> <section-3 title="Syntax"> <div class="context"> </div> </section-3> <section-3 title="Method Signatures"> <div> </div> </section-3> <section-3 title="Arguments and Values"> </section-3> <section-3 title="Description"> </section-3> <section-3 title="Examples"> </section-3> <section-3 title="Affected By"> </section-3> <section-3 title="Exceptional Situations"> </section-3> <section-3 title="See Also"> </section-3> <section-3 title="Notes"> </section-3> </section-2>', '', '', function(opts) {
});

riot.tag2('persistence-sec_root', '<section-2 title="永続化"> </section-2> <section-3 title="概要"> <div class="contents"> <p>永続化は以下の二つのファイルに保管されます。</p> <ol style="margin-left: 33px;"> <li>スナップショット</li> <li>トランザクション・ログ</li> </ol> </div> </section-3> <section-3 title="スナップショット"> <section-4 title="森"> <div class="contents"> <p>memes毎にスナップショットが作成されます。</p> <p>スナップショットを復号化する時に %id のインデックスが作成されます。</p> </div> </section-4> <section-4 title="羅"> <div class="contents"> <p>memes毎にスナップショットが作成されます。</p> <p>from スロットと to スロットは id のみの状態で永続化されます。</p> <p>永続化を復号化するときに森に変換されます。</p> </div> </section-4> </section-3> <section-3 title="トランザクション・ログ"> <div class="contents"> <p>分毎にファイルが作成されます。</p> <p>スナップショットを取得した場合、バックアップフォルダに移動されます。</p> </div> </section-3>', '', '', function(opts) {
});

riot.tag2('persistence', '', '', '', function(opts) {
     this.mixin(MIXINS.page);

     this.on('mount', () => { this.draw(); });
     this.on('update', () => { this.draw(); });
});

riot.tag2('query-sec_root', '<section-2 title="Query"> <section-3 title="概要"> <div class="contents"> <p><pre>\n(q :find :vertex ...)\n(q :find :edge ...)\n(q :get :vertex ...)\n(q :get :edge ...)\n                </pre></p> </div> </section-3> <section-3 title="オペレータ"> <section-4 title="Query"> <table class="table"> <tbody> <tr><td>クエリ</td><th><a href="#query/q">Q</a></th><td></td></tr> </tbody> </table> </section-4> <section-4 title="Finder"> <table class="table"> <tbody> <tr><td>R:検索</td><th>FIND-R</th> <td></td></tr> <tr><td>R:検索</td><th>FIND-R-EDGE</th> <td></td></tr> <tr><td>R:検索</td><th>FIND-R-VERTEX</th> <td></td></tr> <tr><td>R:取得</td><th>GET-R</th> <td></td></tr> <tr><td>R:取得</td><th>GET-R-EDGE</th> <td></td></tr> <tr><td>R:取得</td><th>GET-R-VERTEX</th> <td></td></tr> </tbody> </table> </section-4> </section-3> </section-2> <section-footer></section-footer>', '', '', function(opts) {
});

riot.tag2('query', '', '', '', function(opts) {
     this.mixin(MIXINS.page);

     this.on('mount', () => { this.draw(); });
     this.on('update', () => { this.draw(); });
});

riot.tag2('ra-sec_root', '<section-2 title="Class: RA"> </section-2> <shinrabanshou_class_ra></shinrabanshou_class_ra> <section-footer></section-footer>', '', '', function(opts) {
});

riot.tag2('ra', '', '', '', function(opts) {
     this.mixin(MIXINS.page);

     this.on('mount', () => { this.draw(); });
     this.on('update', () => { this.draw(); });
});

riot.tag2('shin-sec_root', '<section-2 title="Class: SHIN"> </section-2> <shinrabanshou_class_shin></shinrabanshou_class_shin> <section-footer></section-footer>', '', '', function(opts) {
});

riot.tag2('shin', '', '', '', function(opts) {
     this.mixin(MIXINS.page);

     this.on('mount', () => { this.draw(); });
     this.on('update', () => { this.draw(); });
});

riot.tag2('shinrabanshou_slot_edges', '<section-2 title="Slot: EDGES"> <section-3 title="概要"> <div class="contents"> <p>セットされる値はハッシュ表です。</p> <p>ハッシュ表のキーと値は以下の通りです。</p> <table class="table"> <thead> <th></th> <th>Type</th> <th>Description</th> </thead> <tbody> <tr> <th>キー</th> <td>symbol</td> <td>保管する ra のクラスシンボル</td> </tr> <tr> <th>値</th> <td>memes</td> <td>ra を保管する memes</td> </tr> </tbody> </table> </div> </section-3> <section-3 title="保管する shin の インデックス"> <div class="contents"> <p>インデックスを持ちます。 以下の三つです</p> <ol style="margin-left:33px;"> <li>%id</li> <li>from の %id</li> <li>to の %id</li> </ol> </div> </section-3> </section-2> <section-footer></section-footer>', '', '', function(opts) {
});

riot.tag2('shinrabanshou_slot_vertexes', '<section-2 title="Slot: VERTEXES"> <section-3 title="概要"> <div class="contents"> <p>セットされる値はハッシュ表です。</p> <p>ハッシュ表のキーと値は以下の通りです。</p> <table class="table"> <thead> <th></th> <th>Type</th> <th>Description</th> </thead> <tbody> <tr> <th>キー</th> <td>symbol</td> <td>保管する shin のクラスシンボル</td> </tr> <tr> <th>値</th> <td>memes</td> <td>shin を保管する memes</td> </tr> </tbody> </table> </div> </section-3> <section-3 title="保管する shin の インデックス"> <div class="contents"> <p>インデックスを持ちます。%id のみです。</p> </div> </section-3> </section-2> <section-footer></section-footer>', '', '', function(opts) {
});
