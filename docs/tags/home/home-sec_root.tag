<home-sec_root>
    <section-2 title="森羅万象 (SHINRABANSHOU)"
               subtitle="Common Lisp で書かれたグラフデータベース"></section-2>

    <home-tabs tabs={tabs} active={active_tab} callback={clickTab}></home-tabs>

    <section class="section" style="padding-top: 8px;">
        <div class="container">
            <home-sec_readme    type="tab-contents"></home-sec_readme>
            <home-sec_classes   type="tab-contents"></home-sec_classes>
            <home-sec_operators type="tab-contents"></home-sec_operators>
            <home-sec_other     type="tab-contents"></home-sec_other>
        </div>
    </section>

    <section-footer     type="tab-contents"></section-footer>

    <script>
     this.active_tab = 'readme';
     this.tabs = [
         { code: 'readme',    label: 'README' },
         { code: 'classes',   label: 'Classes' },
         { code: 'operators', label: 'Operators' },
         { code: 'other',     label: '他' },
     ];

     this.clickTab = (e) => {
         this.active_tab = e.target.getAttribute('code');
         this.update();
     };

     this.switchTab = () => {
         for (var k in this.tags) {
             tag = this.tags[k];
             if (tag.root.getAttribute('type') != 'tab-contents')
                 continue;
             tag.root.classList.add('hide');
         }

         let code = 'home-sec_' + this.active_tab;

         this.tags[code].root.classList.remove('hide');
     };
     this.on('update', () => {
         this.switchTab();
     });
     this.on('mount', () => {
         this.switchTab();
     });
    </script>
</home-sec_root>
