<home-sec_other>
    <section-4 title="名前の由来">
        <div class="contents">
            <p>日本語の森羅万象が由来です。</p>
            <ol style="margin-left:44px;">
                <li>森羅 : 数多く並びつらなること。また，そのもの。</li>
                <li>万象 : さまざまの形。あらゆる事物。</li>
            </ol>
            <p>キルラキルとは同時期に書き初めたので親近感があります。</p>
        </div>
    </section-4>
    <section-4 title="このドキュメントについて">
        <div class="contents">
            <p>配色はキルラキルから拝借しています。</p>
        </div>
    </section-4>
</home-sec_other>
