<home-sec_classes>
    <section-4 title="List" data={classes()}>
        <div class="contents">
            <class-list data={opts.data}></class-list>
        </div>
    </section-4>

    <section-4 title="Graph">
        <div class="contents">
            <class-diagram></class-diagram>
        </div>
    </section-4>

    <script>
     this.classes = () => {
         return [
             { symbol: 'SHIN',    package: 'SHINRABANSHOU', description: 'Vertex を表わすクラス'},
             { symbol: 'RA',      package: 'SHINRABANSHOU', description: 'Edge を表わすクラス'},
             { symbol: 'BANSHOU', package: 'SHINRABANSHOU', description: 'Shin と Ra を管理するクラス'},
         ];
     };
    </script>
</home-sec_classes>
