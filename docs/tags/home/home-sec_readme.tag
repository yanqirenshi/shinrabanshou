<home-sec_readme>
    <section-3 title="概要">
        <div class="contents">
            <p>Common Lisp で書かれたグラフデータベースです。</p>
            <p>グラフは有向グラフです。</p>
        </div>
    </section-3>

    <section-3 title="目指していること">
        <div class="contents">
            <p>Common Lisp ネイティブでポータブルかつシンプルな GraphDatabase を目指しています。</p>
            <p>外部のDBを利用すれば良いのでしょうが、インストールしたり設定したりと面倒を感じます。</p>
            <ol style="margin-left:44px;">
                <li>シンプルであること。</li>
                <li>関係ライブラリを少なくすること。</li>
                <li>AICD(?) とか難しいことは考えないこと。</li>
                <li>セットアップにストレスを覚えないこと。</li>
                <li>Windows環境でも問題なく利用できること。</li>
            </ol>
        </div>
    </section-3>

    <section-3 title="Author">
        <div class="contents">
            <p>Satoshi Iwasaki (yanqirenshi@gmail.com)</p>
        </div>
    </section-3>

    <section-3 title="Copyright">
        <div class="contents">
            <p>Copyright (c) 2014 Satoshi Iwasaki (yanqirenshi@gmail.com)</p>
        </div>
    </section-3>

    <section-3 title="License">
        <div class="contents">
            <p>Licensed under the LLGPL License.</p>
        </div>
    </section-3>
</home-sec_readme>
