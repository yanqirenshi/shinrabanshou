<home-sec_operators>
    <section-4>
        <div class="contents">
            <p>
                シンプルで規則性のあるオペレータになっています。
            </p>
            <p>
                オペレータは大きく分類すると以下の四つになります。<br/>
            </p>
            <div style="margin-left: 33px;">
                <ol>
                    <li>CROUD</li>
                    <li>Paradicate</li>
                    <li>Accessor</li>
                    <li>Query</li>
                </ol>
            </div>
        </div>
    </section-4>

    <section-4 title="森" data={shin}>
        <div class="contents">
            <operator-list data={opts.data}></operator-list>
        </div>
    </section-4>

    <section-4 title="羅" data={ra}>
        <div class="contents">
            <operator-list data={opts.data}></operator-list>
        </div>
    </section-4>

    <section-4 title="万象" data={banshou}>
        <div class="contents">
            <operator-list data={opts.data}></operator-list>
        </div>
    </section-4>

    <section-4 title="Index" data={index}>
        <div class="contents">
            <operator-list data={opts.data}></operator-list>
        </div>
    </section-4>

    <section-4 title="Query" data={query}>
        <div class="contents">
            <operator-list data={opts.data}></operator-list>
        </div>
    </section-4>

    <script>
     this.shin = [
         { type: 'P:述語', href: '#home/vertexp',          symbol: 'VERTEXP',          package: 'SHINRABANSHOU', description: '' },
         { type: 'C:作成', href: '#home/tx-create-vertex', symbol: 'TX-CREATE-VERTEX', package: 'SHINRABANSHOU', description: '' },
         { type: 'R:検索', href: '#home/find-vertex',      symbol: 'FIND-VERTEX',      package: 'SHINRABANSHOU', description: '' },
         { type: 'R:取得', href: '#home/get-vertex',       symbol: 'GET-VERTEX',       package: 'SHINRABANSHOU', description: '' },
         { type: 'U:更新', href: '#home/tx-update-vertex', symbol: 'TX-UPDATE-VERTEX', package: 'SHINRABANSHOU', description: '' },
         { type: 'D:削除', href: '#home/tx-delete-vertex', symbol: 'TX-DELETE-VERTEX', package: 'SHINRABANSHOU', description: '' },
     ];

     this.ra = [
         { type: 'P:述語', href: null,                      symbol: 'EDGEP',            package: 'SHINRABANSHOU', description: '' },
         { type: 'C:作成', href: '#home/tx-create-edge',    symbol: 'TX-CREATE-EDGE',   package: 'SHINRABANSHOU', description: '' },
         { type: 'R:検索', href: '#home/find-edge',         symbol: 'FIND-EDGE',        package: 'SHINRABANSHOU', description: '' },
         { type: 'R:取得', href: null,                      symbol: 'GET-FROM-VERTEX',  package: 'SHINRABANSHOU', description: '廃棄しようかな。' },
         { type: 'R:取得', href: null,                      symbol: 'GET-TO-VERTEX',    package: 'SHINRABANSHOU', description: '廃棄しようかな。' },
         { type: 'R:取得', href: '#home/get-edge',          symbol: 'GET-EDGE',         package: 'SHINRABANSHOU', description: '' },
         { type: 'U:更新', href: null,                      symbol: '---',              package: 'SHINRABANSHOU', description: 'Edge は汎用的な update 命令はありません。' },
         { type: 'U:更新', href: null,                      symbol: 'TX-CHANGE-VERTEX', package: 'SHINRABANSHOU', description: '' },
         { type: 'U:更新', href: null,                      symbol: 'TX-CHANGE-TYPE',   package: 'SHINRABANSHOU', description: '' },
         { type: 'D:削除', href: '#home/tx-delete-edgexsg', symbol: 'TX-DELETE-EDGE',   package: 'SHINRABANSHOU', description: '' },
     ];

     this.banshou = [
         { type: 'C:作成', href: null, symbol: 'MAKE-BANSHOU',        package: 'SHINRABANSHOU', description: '' },
         { type: '???',    href: null, symbol: 'STATISTICS-VERTEXES', package: 'SHINRABANSHOU', description: 'vertex のクラス一覧を取得します。'  },
         { type: '???',    href: null, symbol: 'STATISTICS-EDGES',    package: 'SHINRABANSHOU', description: 'edge のクラス一覧を取得します。'  },
         { type: '???',    href: null, symbol: 'STATISTICS-INDEXS',   package: 'SHINRABANSHOU', description:  'edge のクラス一覧を取得します。'  },
     ];

     this.query = [
         { type: 'Q:問合', href: null, symbol: 'Q',             package: 'SHINRABANSHOU', description: '' },
         { type: 'R:検索', href: null, symbol: 'FIND-R',        package: 'SHINRABANSHOU', description: '' },
         { type: 'R:検索', href: null, symbol: 'FIND-R-EDGE',   package: 'SHINRABANSHOU', description: '' },
         { type: 'R:検索', href: null, symbol: 'FIND-R-VERTEX', package: 'SHINRABANSHOU', description: '' },
         { type: 'R:取得', href: null, symbol: 'GET-R',         package: 'SHINRABANSHOU', description: '' },
         { type: 'R:取得', href: null, symbol: 'GET-R-EDGE',    package: 'SHINRABANSHOU', description: '' },
         { type: 'R:取得', href: null, symbol: 'GET-R-VERTEX',  package: 'SHINRABANSHOU', description: '' },
     ];

     this.index = [
         { type: 'C:作成', href: null, symbol: 'TX-CREATE-INDEX', package: 'SHINRABANSHOU', description: '' },
         { type: 'R:取得', href: null, symbol: 'GET-INDEX',       package: 'SHINRABANSHOU', description: '' },
         { type: 'U:更新', href: null, symbol: '---',             package: 'SHINRABANSHOU', description: '' },
         { type: 'D:削除', href: null, symbol: 'TX-DELETE-INDEX', package: 'SHINRABANSHOU', description: '' },
         { type: 'U:更新', href: null, symbol: 'REBUILD-INDEX',   package: 'SHINRABANSHOU', description: '' },
     ];


    </script>
</home-sec_operators>
