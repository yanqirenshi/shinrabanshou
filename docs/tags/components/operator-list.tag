<operator-list>
    <table class="table">
        <thead>
            <tr>
                <td>Package</td>
                <td>Symbol</td>
                <td>Type</td>
                <td>Description</td>
            </tr>
        </thead>

        <tbody>
            <tr each={opts.data}>
                <td>{package}</td>
                <td><a href={href}>{symbol}</a></td>
                <td>{type}</td>
                <td>{description}</td>
            </tr>
        </tbody>
    </table>
</operator-list>
