<class-list>
    <table class="table">
        <thead>
            <tr>
                <td>Package</td>
                <td>Symbol</td>
                <td>Description</td>
            </tr>
        </thead>

        <tbody>
            <tr each={opts.data}>
                <td>{package}</td>
                <td>{symbol}</td>
                <td>{description}</td>
            </tr>
        </tbody>
    </table>
</class-list>
