<slot-list>
    <table class="table">
        <thead>
            <tr>
                <td>Name</td>
                <td>Accessor</td>
                <td>Initarg</td>
                <td>Type</td>
                <td>Initform</td>
            </tr>
        </thead>

        <tbody>
            <tr each={opts.data}>
                <td><a href={href ? href : location.hash}>{name}</a></td>
                <td>{accessor}</td>
                <td>{initarg}</td>
                <td>{type}</td>
                <td>{initform}</td>
            </tr>
        </tbody>
    </table>
</slot-list>
