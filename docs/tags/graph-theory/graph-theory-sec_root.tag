<graph-theory-sec_root>
    <section-2 title="グラフ理論入門" subtitle="平面グラフへの応用" data={data}>
    </section-2>

    <graph-theory-tabs tabs={tabs} active={active_tab} callback={clickTab}></graph-theory-tabs>

    <section class="section" style="padding-top: 8px;">
        <div class="container">
            <graph-theory-section01 type="tab-contents"></graph-theory-section01>
            <graph-theory-section02 type="tab-contents"></graph-theory-section02>
            <graph-theory-section03 type="tab-contents"></graph-theory-section03>
            <graph-theory-section04 type="tab-contents"></graph-theory-section04>
            <graph-theory-section05 type="tab-contents"></graph-theory-section05>
            <graph-theory-section06 type="tab-contents"></graph-theory-section06>
            <graph-theory-section07 type="tab-contents"></graph-theory-section07>
            <graph-theory-section08 type="tab-contents"></graph-theory-section08>
        </div>
    </section>

    <section-footer></section-footer>

    <script>
     this.active_tab = 'section01';
     this.tabs = [
         { code: 'section01', label: '背理法と帰納法' },
         { code: 'section02', label: 'グラフと部分グラフ' },
         { code: 'section03', label: 'いろいろなグラフ' },
         { code: 'section04', label: 'マッチング' },
         { code: 'section05', label: '辺彩色' },
         { code: 'section06', label: '独立集合と頂点彩色' },
         { code: 'section07', label: '輸送回路網' },
         { code: 'section08', label: '平面グラフ' },
     ];

     this.clickTab = (e) => {
         this.active_tab = e.target.getAttribute('code');
         this.update();
     };

     this.switchTab = () => {
         for (var k in this.tags) {
             tag = this.tags[k];
             if (tag.root.getAttribute('type') != 'tab-contents')
                 continue;
             tag.root.classList.add('hide');
         }

         let code = 'graph-theory-' + this.active_tab;
         this.tags[code].root.classList.remove('hide');
     };
     this.on('update', () => {
         this.switchTab();
     });
     this.on('mount', () => {
         this.switchTab();
     });

    </script>
</graph-theory-sec_root>
