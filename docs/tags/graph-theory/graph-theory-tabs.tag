<graph-theory-tabs>
    <section class="section" style="padding-top: 8px; padding-bottom: 8px;">
        <div class="container">
            <div class="tabs is-boxed">
                <ul>
                    <li each={opts.tabs}
                        class="{opts.active==code ? 'is-active' : ''}">
                        <a code={code}
                           onclick={opts.callback}>{label}</a>
                    </li>
                </ul>
            </div>
        </div>
    </section>

    <style>
     graph-theory-tabs li:first-child {
         margin-left: 22px;
     }
    </style>
</graph-theory-tabs>
