<graph-theory-section08>
    <section-3 title="オイラーの公式">
    </section-3>

    <section-3 title="クラトウスキーの定理">
    </section-3>

    <section-3 title="双対グラフ">
    </section-3>

    <section-3 title="4色問題">
    </section-3>

    <section-3 title="平面グラフの直線描画">
    </section-3>

    <section-3 title="直線描画アルゴリズム">
    </section-3>

    <section-3 title="平面性判定">
    </section-3>

    <section-3 title="3連結判定アルゴリズム">
    </section-3>

    <section-3 title="凸多面体グラフ">
    </section-3>
</graph-theory-section08>
