<query-sec_root>
    <section-2 title="Query">
        <section-3 title="概要">
            <div class="contents">
                <p><pre>
(q :find :vertex ...)
(q :find :edge ...)
(q :get :vertex ...)
(q :get :edge ...)
                </pre></p>
            </div>
        </section-3>

        <section-3 title="オペレータ">
            <section-4 title="Query">
                <table class="table">
                    <tbody>
                        <tr><td>クエリ</td><th><a href="#query/q">Q</a></th><td></td></tr>
                    </tbody>
                </table>
            </section-4>
            <section-4 title="Finder">
                <table class="table">
                    <tbody>
                        <tr><td>R:検索</td><th>FIND-R</th>        <td></td></tr>
                        <tr><td>R:検索</td><th>FIND-R-EDGE</th>   <td></td></tr>
                        <tr><td>R:検索</td><th>FIND-R-VERTEX</th> <td></td></tr>
                        <tr><td>R:取得</td><th>GET-R</th>         <td></td></tr>
                        <tr><td>R:取得</td><th>GET-R-EDGE</th>    <td></td></tr>
                        <tr><td>R:取得</td><th>GET-R-VERTEX</th>  <td></td></tr>
                    </tbody>
                </table>
            </section-4>
        </section-3>
    </section-2>

    <section-footer></section-footer>
</query-sec_root>
