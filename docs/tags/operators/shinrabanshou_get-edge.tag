<shinrabanshou_get-edge>
    <section-2 title="Generic Function: TX-CREATE-EDGE">
        <section-3 title="Syntax">
            <div class="context">
                <p>get-edge graph class-symbol &key %id</p>
            </div>
        </section-3>

        <section-3 title="Method Signatures">
            <div class="context">
                <p>(graph banshou) (class-symbol symbol) &key %id</p>
            </div>
        </section-3>

        <section-3 title="Arguments and Values">
        </section-3>

        <section-3 title="Description">
        </section-3>

        <section-3 title="Examples">
        </section-3>

        <section-3 title="Affected By">
        </section-3>

        <section-3 title="Exceptional Situations">
        </section-3>

        <section-3 title="See Also">
        </section-3>

        <section-3 title="Notes">
        </section-3>
    </section-2>
</shinrabanshou_get-edge>
