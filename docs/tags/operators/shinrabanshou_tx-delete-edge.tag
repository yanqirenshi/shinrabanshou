<shinrabanshou_tx-delete-edge>
    <section-2 title="Generic Function: TX-CREATE-EDGE">
        <section-3 title="Syntax">
            <div class="context">
                <p>tx-delete-edge graph edge)</p>
            </div>
        </section-3>

        <section-3 title="Method Signatures">
            <div>
                <p>(graph banshou) (edge ra)</p>
            </div>
        </section-3>

        <section-3 title="Arguments and Values">
        </section-3>

        <section-3 title="Description">
        </section-3>

        <section-3 title="Examples">
        </section-3>

        <section-3 title="Affected By">
        </section-3>

        <section-3 title="Exceptional Situations">
        </section-3>

        <section-3 title="See Also">
        </section-3>

        <section-3 title="Notes">
        </section-3>
    </section-2>
</shinrabanshou_tx-delete-edge>
