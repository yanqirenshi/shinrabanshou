<shinrabanshou_tx-create-edge>
    <section-2 title="Generic Function: TX-CREATE-EDGE">
        <section-3 title="Syntax">
            <div class="contents">
                <p>tx-create-edge graph class from to type &optional slots-and-values</p>
            </div>
        </section-3>

        <section-3 title="Method Signatures">
            <div class="contents">
                <p>(graph banshou) (class symbol) (from shin) (to shin) (type symbol) &optional slots-and-values</p>
            </div>
        </section-3>

        <section-3 title="Arguments and Values">
            <section-5 title="graph"></section-5>
            <section-5 title="class"></section-5>
            <section-5 title="from"></section-5>
            <section-5 title="to"></section-5>
            <section-5 title="tyoe"></section-5>
            <section-5 title="params"></section-5>
        </section-3>

        <section-3 title="Description">
            <div class="contents">
                <p>RA を作成します。</p>
                <p>BANSHOU に RA を追加します。</p>
                <p>
                    BANSHOU に RA の EDGES(MEMES) が存在しない場合、EDGES(MEMES) を自動で追加します。<br/>
                    その際に <code>%id</code>、 <code>from.%id</code>、 <code>to.%id</code> のインデックスも作成されます。
                </p>
            </div>
        </section-3>

        <section-3 title="Examples">
        </section-3>

        <section-3 title="Affected By">
        </section-3>

        <section-3 title="Exceptional Situations">
        </section-3>

        <section-3 title="See Also">
        </section-3>

        <section-3 title="Notes">
        </section-3>
    </section-2>
</shinrabanshou_tx-create-edge>
