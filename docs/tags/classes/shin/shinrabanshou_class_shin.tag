<shinrabanshou_class_shin>
    <section class="section" style="padding-top:11px;">
        <div class="container">
            <page-tabs core={page_tabs} callback={clickTab}></page-tabs>

            <div style="margin-top:33px;">
                <shinrabanshou_class_shin_readme class="hide"></shinrabanshou_class_shin_readme>
                <shinrabanshou_class_shin_operators class="hide"></shinrabanshou_class_shin_operators>
                <shinrabanshou_class_shin_slots  class="hide"></shinrabanshou_class_shin_slots>
            </div>
        </div>
    </section>


    <script>
     this.page_tabs = new PageTabs([
         {code: 'readme',   label: 'README',   tag: 'shinrabanshou_class_shin_readme' },
         {code: 'slots',    label: 'SLOTS',    tag: 'shinrabanshou_class_shin_slots' },
         {code: 'oprators', label: 'OPRATORS', tag: 'shinrabanshou_class_shin_operators' },
     ]);

     this.on('mount', () => {
         this.page_tabs.switchTab(this.tags)
         this.update();
     });

     this.clickTab = (e, action, data) => {
         if (this.page_tabs.switchTab(this.tags, data.code))
             this.update();
     };
    </script>
</shinrabanshou_class_shin>
