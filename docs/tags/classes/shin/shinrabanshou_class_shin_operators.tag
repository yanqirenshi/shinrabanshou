<shinrabanshou_class_shin_operators>
    <section-3 title="Operators:" operators={operators()}>
        <div class="contents">
            <p>森(Vertex)の操作</p>
            <operator-list data={opts.operators}></operator-list>
        </div>
    </section-3>

    <section-footer></section-footer>

    <script>
     this.operators = () => {
         return [
             { type: 'P:述語', href: '#shin/vertexp',          symbol: 'VERTEXP',          package: 'SHINRABANSHOU', description: '' },
             { type: 'C:作成', href: '#shin/tx-create-vertex', symbol: 'TX-CREATE-VERTEX', package: 'SHINRABANSHOU', description: '' },
             { type: 'R:検索', href: '#shin/find-vertex',      symbol: 'FIND-VERTEX',      package: 'SHINRABANSHOU', description: '' },
             { type: 'R:取得', href: '#shin/get-vertex',       symbol: 'GET-VERTEX',       package: 'SHINRABANSHOU', description: '' },
             { type: 'U:更新', href: '#shin/tx-update-vertex', symbol: 'TX-UPDATE-VERTEX', package: 'SHINRABANSHOU', description: '' },
             { type: 'D:削除', href: '#shin/tx-delete-vertex', symbol: 'TX-DELETE-VERTEX', package: 'SHINRABANSHOU', description: '' },
         ];
     };
    </script>
</shinrabanshou_class_shin_operators>
