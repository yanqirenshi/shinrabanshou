<shinrabanshou_class_ra>
    <section class="section" style="padding-top:11px;">
        <div class="container">
            <page-tabs core={page_tabs} callback={clickTab}></page-tabs>

            <div style="margin-top:33px;">
                <shinrabanshou_class_ra_readme class="hide"></shinrabanshou_class_ra_readme>
                <shinrabanshou_class_ra_operators class="hide"></shinrabanshou_class_ra_operators>
                <shinrabanshou_class_ra_slots  class="hide"></shinrabanshou_class_ra_slots>
            </div>
        </div>
    </section>

    <script>
     this.page_tabs = new PageTabs([
         {code: 'readme',   label: 'README',   tag: 'shinrabanshou_class_ra_readme' },
         {code: 'slots',    label: 'SLOTS',    tag: 'shinrabanshou_class_ra_slots' },
         {code: 'oprators', label: 'OPRATORS', tag: 'shinrabanshou_class_ra_operators' },
     ]);

     this.on('mount', () => {
         this.page_tabs.switchTab(this.tags)
         this.update();
     });

     this.clickTab = (e, action, data) => {
         if (this.page_tabs.switchTab(this.tags, data.code))
             this.update();
     };
    </script>
</shinrabanshou_class_ra>
