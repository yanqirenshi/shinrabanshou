<shinrabanshou_class_ra_operators>
    <section-3 title="Operators:" data={data}>
        <section-4 title="Basic" data={opts.data}>
            <div class="contents">
                <p>羅(Edge)の基本操作</p>
                <operator-list data={opts.data.operators.basic}></operator-list>
            </div>
        </section-4>

        <section-4 title="Slots" data={opts.data}>
            <div class="contents">
                <operator-list data={opts.data.operators.slots}></operator-list>
            </div>
        </section-4>
    </section-3>

    <script>
     this.operators_basic = [
         { type: 'P:述語', href: null,                    symbol: 'EDGEP',          package: 'SHINRABANSHOU', description: '' },
         { type: 'C:作成', href: '#ra/tx-create-edge',    symbol: 'TX-CREATE-EDGE', package: 'SHINRABANSHOU', description: '' },
         { type: 'R:検索', href: '#ra/find-edge',         symbol: 'FIND-EDGE',      package: 'SHINRABANSHOU', description: '' },
         { type: 'R:取得', href: '#ra/get-edge',          symbol: 'GET-EDGE',       package: 'SHINRABANSHOU', description: '' },
         { type: 'U:更新', href: null,                    symbol: '---',            package: 'SHINRABANSHOU', description: '' },
         { type: 'D:削除', href: '#ra/tx-delete-edgexsg', symbol: 'TX-DELETE-EDGE', package: 'SHINRABANSHOU', description: '' },
     ];

     this.operators_slots = [
         { type: 'R:取得', href: null, symbol: 'GET-FROM-VERTEX',  package: 'SHINRABANSHOU', description: '' },
         { type: 'R:取得', href: null, symbol: 'GET-TO-VERTEX',    package: 'SHINRABANSHOU', description: '' },
         { type: 'U:更新', href: null, symbol: 'TX-CHANGE-VERTEX', package: 'SHINRABANSHOU', description: '' },
         { type: 'U:更新', href: null, symbol: 'TX-CHANGE-TYPE',   package: 'SHINRABANSHOU', description: '' },
     ];

     this.data = {
         operators: {
             basic: this.operators_basic,
             slots: this.operators_slots,
         }
     };
    </script>
</shinrabanshou_class_ra_operators>
