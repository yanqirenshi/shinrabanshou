<shinrabanshou_class_ra_slots>
    <section-3 title="Slots:" data={slots}>
        <div class="contents">
            <slot-list data={opts.data}></slot-list>
        </div>
    </section-3>

    <script>
     this.slots = [
         { name: 'EDGE-TYPE',  accessor: 'edge-type',  initarg: 'edge-type',  type: 'symbol (keyword)', initform: 'nil', href: '#ra/edge-type' },
         { name: 'FROM',       accessor: 'from',       initarg: 'from',       type: 'shin',             initform: 'nil', href: '#ra/from' },
         { name: 'FROM-ID',    accessor: 'from-id',    initarg: 'from-id',    type: 'Integer',          initform: 'nil', },
         { name: 'FROM-CLASS', accessor: 'from-class', initarg: 'from-class', type: 'Symbol (class)',   initform: 'nil', },
         { name: 'TO',         accessor: 'to',         initarg: 'to',         type: 'shin',             initform: 'nil', href: '#ra/to' },
         { name: 'TO-ID',      accessor: 'to-id',      initarg: 'to-id',      type: 'Integer',          initform: 'nil', },
         { name: 'TO-CLASS',   accessor: 'to-class',   initarg: 'to-class',   type: 'Symbol (class)',   initform: 'nil', },
     ];
    </script>
</shinrabanshou_class_ra_slots>
