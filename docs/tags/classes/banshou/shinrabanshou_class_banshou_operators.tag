<shinrabanshou_class_banshou_operators>
    <section-3 title="Operators:" data={data}>
        <section-4 title="Basic" data={opts.data}>
            <div class="contents">
                <p>羅(Edge)の基本操作</p>
                <operator-list data={opts.data.operators.basic}></operator-list>
            </div>
        </section-4>

        <section-4 title="Statistics" data={opts.data}>
            <div class="contents">
                <operator-list data={opts.data.operators.statistics}></operator-list>
            </div>
        </section-4>
    </section-3>

    <section-footer></section-footer>

    <script>
     this.operators_basic = [
         { type: 'C:作成', href: null, symbol: 'MAKE-BANSHOU',  package: 'SHINRABANSHOU', description: '' },
     ];

     this.operators_statistics = [
         { type: '???', href: null, symbol: 'STATISTICS-VERTEXES', package: 'SHINRABANSHOU', description: 'vertex のクラス一覧を取得します。'  },
         { type: '???', href: null, symbol: 'STATISTICS-EDGES',    package: 'SHINRABANSHOU', description: 'edge のクラス一覧を取得します。'  },
         { type: '???', href: null, symbol: 'STATISTICS-INDEXS',   package: 'SHINRABANSHOU', description:  'edge のクラス一覧を取得します。'  },
     ];

     this.data = {
         operators: {
             basic: this.operators_basic,
             statistics: this.operators_statistics,
         }
     };
    </script>
</shinrabanshou_class_banshou_operators>
