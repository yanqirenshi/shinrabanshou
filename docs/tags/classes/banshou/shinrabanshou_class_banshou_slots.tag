<shinrabanshou_class_banshou_slots>
    <section-3 title="Slots:" data={slots}>
        <div class="contents">
            <slot-list data={opts.data}></slot-list>
        </div>
    </section-3>

    <script>
     this.slots = [
         { name: 'VERTEXES',  accessor: 'vertexes',  initarg: '---',  type: 'hash-table', initform: '(make-hash-table :test \'eq)', href: '#banshou/vertexes' },
         { name: 'EDGES',  accessor: 'edges',  initarg: '(make-hash-table :test \'eq)',  type: 'hash-table', initform: '---', href: '#banshou/edges' },
     ];
    </script>
</shinrabanshou_class_banshou_slots>
