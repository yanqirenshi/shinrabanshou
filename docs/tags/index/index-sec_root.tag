<index-sec_root>
    <section-2 title="Index">
        <section-3 title="概要">
        </section-3>

        <section-3 title="オペレータ">
            <section-4 title="Basic">
                <div class="contents">
                    <table class="table">
                        <tbody>
                            <tr><td>C:作成</td><th>TX-CREATE-INDEX</th> <td></td></tr>
                            <tr><td>R:取得</td><th>GET-INDEX</th>        <td></td></tr>
                            <tr><td>U:更新</td><th>---</th>         <td></td></tr>
                            <tr><td>D:削除</td><th>TX-DELETE-INDEX</th>   <td></td></tr>
                        </tbody>
                    </table>
                </div>
            </section-4>
            <section-4 title="Maintains">
                <div class="contents">
                    <table class="table">
                        <tbody>
                            <tr><td>U:更新</td><th>REBUILD-INDEX</th>         <td></td></tr>
                        </tbody>
                    </table>
                </div>
            </section-4>
        </section-3>
    </section-2>

    <section-footer></section-footer>
</index-sec_root>
