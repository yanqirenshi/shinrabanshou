<section-header>
    <section class="section">
        <div class="container">
            <h1 class="title is-{opts.no ? opts.no : 3}">
                {opts.title}
            </h1>
            <h2 class="subtitle">{opts.subtitle}</h2>

            <yield/>
        </div>
    </section>

    <style>
     section-header > .section {
         padding-top: 13px;
         padding-bottom: 13px;
         height: 88px;
         background: #273C57;
         margin-bottom: 11px;
     }
     section-header .title {
         color: #D6340E;
         font-weight: bold;
     }
     section-header .subtitle {
         color: #FDFFFE;
         font-weight: bold;
     }
    </style>
</section-header>
