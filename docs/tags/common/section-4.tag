<section-4>
    <section class="section">
        <div class="container">
            <h1 class="title is-4">{opts.title}</h1>
            <yield/>
        </div>
    </section>

    <style>
     section-4 .section {
         padding-top: 11px;
         padding-bottom: 33px;
     }
     section-4 .contents {
         padding-left: 33px;
     }
    </style>
</section-4>
