<section-3>
    <section class="section">
        <div class="container">
            <h1 class="title is-3">{opts.title}</h1>
            <yield/>
        </div>
    </section>

    <style>
     section-3 .section {
         padding-top: 11px;
         padding-bottom: 33px;
     }
     section-3 > .section > .container > .title {
         color: #BC361D;
         font-weight: bold;
     }
     section-3 .contents {
         padding-left: 33px;
     }
    </style>
</section-3>
