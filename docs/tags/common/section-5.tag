<section-5>
    <section class="section">
        <div class="container">
            <h1 class="title is-5">{opts.title}</h1>
            <yield/>
        </div>
    </section>

    <style>
     section-5 .section {
         padding-top: 11px;
         padding-bottom: 33px;
     }
     section-5 .contents {
         padding-left: 33px;
     }
    </style>
</section-5>
