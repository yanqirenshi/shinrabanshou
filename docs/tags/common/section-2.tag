<section-2>
    <section class="section">
        <div class="container">
            <h1 class="title is-2">{opts.title}</h1>
            <h2 class="subtitle">{opts.subtitle}</h2>
        </div>
    </section>
    <div class="black"></div>
    <div class="orange"></div>
    <div class="black"></div>
    <div class="yelow"></div>
    <div class="black"></div>
    <div class="red"></div>
    <div class="black tail"></div>

    <yield/>

    <style>
     section-2 > .section {
         background: #2C3C53;
         padding: 2rem 1.5rem;
     }
     section-2 > .section > .container > .title {
         color: #D6340E;
         font-weight: bold;
     }
     section-2 .orange {
         height: 3px;
         background: #FF691F;
         width: 100%;
     }
     section-2 .yelow {
         height: 3px;
         background: #FDF579;
         width: 100%;
     }
     section-2 .red {
         height: 3px;
         background: #C22252;
         width: 100%;
     }
     section-2 .black {
         background: #3B000D;
         height: 1px;
     }
     section-2 .black.tail {
         margin-bottom: 33px;
     }
    </style>
</section-2>
