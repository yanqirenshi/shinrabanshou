<persistence-sec_root>
    <section-2 title="永続化">
    </section-2>

    <section-3 title="概要">
        <div class="contents">
            <p>永続化は以下の二つのファイルに保管されます。</p>
            <ol style="margin-left: 33px;">
                <li>スナップショット</li>
                <li>トランザクション・ログ</li>
            </ol>
        </div>
    </section-3>

    <section-3 title="スナップショット">
        <section-4 title="森">
            <div class="contents">
                <p>memes毎にスナップショットが作成されます。</p>
                <p>スナップショットを復号化する時に %id のインデックスが作成されます。</p>
            </div>
        </section-4>
        <section-4 title="羅">
            <div class="contents">
                <p>memes毎にスナップショットが作成されます。</p>
                <p>from スロットと to スロットは id のみの状態で永続化されます。</p>
                <p>永続化を復号化するときに森に変換されます。</p>
            </div>
        </section-4>
    </section-3>

    <section-3 title="トランザクション・ログ">
        <div class="contents">
            <p>分毎にファイルが作成されます。</p>
            <p>スナップショットを取得した場合、バックアップフォルダに移動されます。</p>
        </div>
    </section-3>
</persistence-sec_root>
