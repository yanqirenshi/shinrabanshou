<shinrabanshou_slot_vertexes>
    <section-2 title="Slot: VERTEXES">
        <section-3 title="概要">
            <div class="contents">
                <p>セットされる値はハッシュ表です。</p>
                <p>ハッシュ表のキーと値は以下の通りです。</p>
                <table class="table">
                    <thead>
                        <th></th>
                        <th>Type</th>
                        <th>Description</th>
                    </thead>

                    <tbody>
                        <tr>
                            <th>キー</th>
                            <td>symbol</td>
                            <td>保管する shin のクラスシンボル</td>
                        </tr>
                        <tr>
                            <th>値</th>
                            <td>memes</td>
                            <td>shin を保管する memes</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </section-3>

        <section-3 title="保管する shin の インデックス">
            <div class="contents">
                <p>インデックスを持ちます。%id のみです。</p>
            </div>
        </section-3>
    </section-2>

    <section-footer></section-footer>
</shinrabanshou_slot_vertexes>
