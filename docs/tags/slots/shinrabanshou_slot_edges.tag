<shinrabanshou_slot_edges>
    <section-2 title="Slot: EDGES">
        <section-3 title="概要">
            <div class="contents">
                <p>セットされる値はハッシュ表です。</p>
                <p>ハッシュ表のキーと値は以下の通りです。</p>
                <table class="table">
                    <thead>
                        <th></th>
                        <th>Type</th>
                        <th>Description</th>
                    </thead>

                    <tbody>
                        <tr>
                            <th>キー</th>
                            <td>symbol</td>
                            <td>保管する ra のクラスシンボル</td>
                        </tr>
                        <tr>
                            <th>値</th>
                            <td>memes</td>
                            <td>ra を保管する memes</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </section-3>

        <section-3 title="保管する shin の インデックス">
            <div class="contents">
                <p>インデックスを持ちます。 以下の三つです</p>
                <ol style="margin-left:33px;">
                    <li>%id</li>
                    <li>from の %id</li>
                    <li>to の %id</li>
                </ol>
            </div>
        </section-3>
    </section-2>

    <section-footer></section-footer>
</shinrabanshou_slot_edges>
