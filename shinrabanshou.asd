(in-package :cl-user)

(defpackage shinrabanshou-asd
  (:use :cl :asdf))
(in-package :shinrabanshou-asd)

(defsystem shinrabanshou
  :version "0.1"
  :author "Satoshi Iwasaki"
  :license "LLGPL"
  :depends-on (:alexandria
               :cl-ppcre
               :upanishad)
  :components ((:module "src"
                :components
                ((:file "conditions")
                 (:file "utility")
                 (:file "package")
                 (:file "class")
                 (:file "predicate")
                 (:module "vertex"
                  :components ((:file "condition")
                               (:file "predicate")
                               (:file "read")
                               (:file "create")
                               (:file "delete")
                               (:file "update")))
                 (:module "edge"
                  :components ((:file "predicate")
                               (:file "read")
                               (:file "update")
                               (:file "create")
                               (:file "delete")))
                 (:module "finder"
                  :components ((:file "get")
                               (:file "find")))
                 (:module "banshou"
                  :components ((:file "make")
                               (:file "vertexes")
                               (:file "edges")
                               (:file "statistics")))
                 (:module "persistence"
                  :components ((:file "package")
                               (:file "util")
                               ;; (:file "shin")
                               ;; (:file "ra")
                               )))))
  :description ""
  :long-description
  #.(with-open-file (stream (merge-pathnames
                             #p"README.markdown"
                             (or *load-pathname* *compile-file-pathname*))
                            :if-does-not-exist nil
                            :direction :input)
      (when stream
        (let ((seq (make-array (file-length stream)
                               :element-type 'character
                               :fill-pointer t)))
          (setf (fill-pointer seq) (read-sequence seq stream))
          seq)))
  :in-order-to ((test-op (load-op shinrabanshou-test))))
