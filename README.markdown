# 森羅万象(shinrabanshou)

Common Lisp On-Memory Objective Graph Database.

Common Lisp ネイティブでセットアップ/利用にストレス感じさせないシンプルな GraphDatabase を目指しています。

# Usage

``` lisp
```

# Installation
``` lisp
(ql:quickload :shinrabanshou)
```

``` lisp
(ql:quickload :shinrabanshou-test)
```

# Author

+ Satoshi Iwasaki (yanqirenshi@gmail.com)

# Copyright

Copyright (c) 2014 Satoshi Iwasaki (yanqirenshi@gmail.com)

# License

Licensed under the LLGPL License.
